require File.join(WEBLIB_DIR, "rb/myhash")

class App < AppBase
  def initialize
    super()
    init()
  end

  def get_groups
    groups_file = data_path("groups.json")
    if File.exist?(groups_file)
      JSON.parse(File.read(groups_file))
    else
      []
    end
  end

  # --------------------------------

  def init
    get "/" do |req, params|
      [
        200,
        { "Content-Type" => "text/html" },
        <<~HTML
          <head>
            <meta charset="utf-8" />
          </head>
          <pre>
          <a href="/tables/">テーブル一覧</a>
          </pre>
        HTML
      ]
    end

    # --------------------------------

    get "/tables/" do |req, params|
      [
        200,
        { "Content-Type" => "text/html" },
        render_nofile("/page_tdef_index")
      ]
    end

    get "/api/tables/" do |req, params|
      _api_v2(params) do |_params, _|
        db = get_db()
        dao = TdefDao.new(db)

        scond = {}
        scond[:g] = _params[:g]

        tdefs = dao.search(scond)

        # TODO 暫定
        tdefs.sort! { |a, b|
          k_a = "#{ a.group_id } #{ a.pname }"
          k_b = "#{ b.group_id } #{ b.pname }"
          k_a <=> k_b
        }

        groups = get_groups()

        {
          tables: tdefs.map(&:to_plain),
          search_cond: {
            orders: []
          },
          groups: groups
        }
      end
    end

    post "/api/tables/" do |req, params|
      _api_v2(params) do |_params, _|
        db = get_db()
        dao = TdefDao.new(db)

        dao.create()

        {}
      end
    end

    # --------------------------------

    get "/tables/:id" do |req, params|
      # tdef_id = params["id"].to_i

      [
        200,
        { "Content-Type" => "text/html" },
        render_nofile("/page_tdef_show")
      ]
    end

    get "/api/tables/:id" do |req, params|
      tdef_id = params["id"].to_i
      pp_e [108, tdef_id]

      _api_v2(params) do |_params, _|
        db = get_db()
        dao = TdefDao.new(db)
        cdef_dao = CdefDao.new(db)

        tdef = dao.get(tdef_id)

        cdefs = cdef_dao.get_for_table(tdef_id)
        cdefs.each { |cdef|
          cdef[:json] = '{ "pk": false }' # TODO
        }

        using_tdefs = dao.get_using_tdefs(tdef_id)

        groups = get_groups()

        {
          table: tdef.to_plain(),
          cdefs_old: [],
          cdefs: cdefs,
          tag_names: [],
          using_tdefs: using_tdefs,
          ddl: "{ddl}",
          groups: groups
        }
      end
    end

    get "/tables/:id/edit" do |req, params|
      # tdef_id = params["id"].to_i

      [
        200,
        { "Content-Type" => "text/html" },
        render_nofile("/page_tdef_edit")
      ]
    end

    # TODO => put
    patch "/api/tables/:id" do |req, params|
      _api_v2(params) do |_params, _|
        pp_e [244, params, _params]

        tdef =
          Tdef.from_plain(
            _params[:tdef].merge(
              {
                ctime: "TODO",
                utime: Time.now.strftime("%F_%T")
              }
            )
          )

        db = get_db()
        dao = TdefDao.new(db)
        dao.update(tdef)

        {}
      end
    end

    # --------------------------------

    post "/api/tables/:tid/columns/" do |req, params|
      tdef_id = params["tid"].to_i

      _api_v2(params) do |_params, _|
        db = get_db()
        dao = CdefDao.new(db)

        new_id = dao.max_id(tdef_id) + 1
        # pp_e [127, new_id]

        cdef =
          if _params[:cdef]
            _params[:cdef][:ctime] = "2021-01-01_12:00:00" # TODO
            _params[:cdef][:utime] = "2021-01-01_12:00:00" # TODO
            Cdef.from_plain(_params[:cdef])
          else
            nil
          end

        ret = dao.insert(tdef_id, new_id, cdef)

        ret
      end
    end

    get "/tables/:tid/columns/:cid/edit" do |req, params|
      # tdef_id = params["tid"].to_i
      # cdef_id = params["cid"].to_i

      [
        200,
        { "Content-Type" => "text/html" },
        render_nofile("/page_cdef_edit")
      ]
    end

    get "/api/tables/:tid/columns/:cid" do |req, params|
      tdef_id = params["tid"].to_i
      cdef_id = params["cid"].to_i

      _api_v2(params) do |_params, _|
        db = get_db()
        dao = CdefDao.new(db)
        tdef_dao = TdefDao.new(db)

        cdef = dao.get(tdef_id, cdef_id)
        pp_e [156, cdef]
        tdef = tdef_dao.get(tdef_id)

        {
          cdef: cdef.to_plain(),
          tdef: tdef.to_plain()
        }
      end
    end

    delete "/api/tables/:tid/columns/:cid" do |req, params|
      tdef_id = params["tid"].to_i
      cdef_id = params["cid"].to_i

      _api_v2(params) do |_params, _|
        db = get_db()
        dao = CdefDao.new(db)

        dao.delete(tdef_id, cdef_id)

        {}
      end
    end

    # TODO => put
    patch "/api/tables/:tid/columns/:cid" do |req, params|
      tdef_id = params["tid"].to_i
      cdef_id = params["cid"].to_i

      _api_v2(params) do |_params, _|
        pp_e [189, _params]

        db = get_db()
        dao = CdefDao.new(db)

        cdef = Cdef.from_plain(
          _params[:cdef]
            .merge(
              {
                ctime: "TODO",
                utime: Time.now.strftime("%F_%T")
              }
            )
        )
        pp_e [191, cdef]

        dao.update(tdef_id, cdef_id, cdef)

        {}
      end
    end

    get "/api/rpc" do |req, params|
      _api_v2(params) do |_params, _|
        case _params[:action]
        when "fksearch"
          cdefs = fksearch(_params[:kw])
          {
            cdefs: cdefs
          }
        when "diff_tdef"
          diff, diff_cols, input = rpc_diff_tdef(
            tdef: _params[:tdef],
            cdefs: _params[:cdefs],
            tdef_other: _params[:tdef_other]
          )

          {
            diff: diff,
            diff_cols: diff_cols,
            input: input
          }
        when "patch_cdef"
          rpc_patch_cdef(_params)
        else
          raise "invalid action (#{ _params[:action] })"
        end
      end
    end
  end

  def rpc_patch_cdef(_params)
    tdef_id = _params[:tdef_id]
    cdef_id = _params[:cdef_id]
    order = _params[:cdef][:order]

    db = get_db()
    dao = CdefDao.new(db)

    cdef = dao.get(tdef_id, cdef_id)
    new_cdef = cdef.merge(_params[:cdef])

    dao.update(tdef_id, cdef_id, new_cdef)
  end

  def fksearch(kw)
    db = get_db()
    dao = TdefDao.new(db)

    dao.search_for_fk(kw)
  end

  def rpc_diff_tdef(
    tdef:,
    cdefs:,
    tdef_other:
  )
    # pp_e [637, tdef, cdefs]

    data_cols =
      cdefs
        .sort_by { |cdef| cdef[:order] }
        .map { |cdef|
          _cdef = Cdef.from_plain(
            cdef.merge(
              {
                memo_sub: "TODO"
              }
            )
          )
          {
            order: _cdef.order,
            pname:    _cdef.pname,
            name:     _cdef.name,
            type:     _cdef.type,
            type_param: _cdef.type_param,
            pk:       _cdef.pk,
            unsigned: _cdef.unsigned,
            not_null: _cdef.not_null,
            default_value: _cdef.default_value,
            memo:     _cdef.memo
          }
        }

    # make data self
    data_self = {
      cdefs: data_cols
    }

    # make data other
    TdefCompare.compare(data_self, tdef_other)
  end
end
