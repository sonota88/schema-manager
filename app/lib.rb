class MyServlet < WEBrick::HTTPServlet::AbstractServlet
  def reload
    load "lib.rb"
    load "lib/dao/dao_file.rb"
    load "lib/tdef_compare.rb"
    load "app.rb"
  end

  def do_GET(req, res)
    reload()
    App.new.do_get(req, res)
  end

  def do_POST(req, res)
    reload()
    App.new.do_post(req, res)
  end
end

# --------------------------------

class Routing
  def initialize
    @routes = []
  end

  def add(method, pattern, block)
    @routes << {
      pattern: pattern,
      method: method,
      block: block
    }
  end

  def match(method, path_target)
    @routes.each do |route|
      path_pattern = route[:pattern]
      parts_p = path_pattern.split("/", -1)
      parts_t = path_target.split("/", -1)
      size = [
        parts_p.size,
        parts_t.size
      ].max

      params = {}
      matched = true

      (0...size).each do |i|
        part_p = parts_p[i]
        part_t = parts_t[i]

        if part_p.nil?
          matched = false
          break
        elsif part_p.start_with?(":")
          key = part_p[1..-1]
          params[key] = part_t
        else
          if part_t != part_p
            matched = false
            break
          end
        end
      end

      if method != route[:method]
        matched = false
      end

      if matched
        return [route, params]
      end
    end

    nil
  end

  def match?(method, path_target)
    match(method, path_target)
  end

  def to_params(req)
    params = {}
    req.query.each { |k, v|
      params[k.to_sym] = v
    }
    params
  end

  def dispatch(route, req, params)
    params = params.merge(to_params(req))
    route[:block].call(req, params)
  end
end

# --------------------------------

class AppBase
  def initialize
    @routing = Routing.new
  end

  def read_view_file(tail)
    view_dir = app_path("views/")
    File.read(File.join(view_dir, tail))
  end

  def render(path)
    [
      read_view_file("_header.html"),
      read_view_file("#{path}.html"),
      read_view_file("_footer.html")
    ].join("")
  end

  def render_nofile(js_path)
    body = <<~EOB
      <div id="tree_builder_container"></div>
      <script src="/js#{js_path}.js"></script>
    EOB

    [
      read_view_file("_header.html"),
      body,
      read_view_file("_footer.html")
    ].join("")
  end

  def redirect_to(path)
    <<~HTML
      <script>
        location.href = "#{path}";
      </script>
    HTML
  end

  def to_api_triple(json)
    [
      200,
      { "Content-Type" => "application/json" },
      json
    ]
  end

  def _api_v2(params)
    result = {}
    context = {
      :errors => []
    }

    begin
      api_params =
        Myhash.new( JSON.parse(params[:_params]) )
          .to_snake
          .to_sym_key
          .to_plain
      pp_e api_params if $PROFILE == :devel
      result = yield(api_params, context)
    rescue => e
      $stderr.puts e.class, e.message, e.backtrace
      context[:errors] << {
        :msg => "#{e.class}: #{e.message}",
        :trace => e.backtrace.join("\n")
      }
    end

    result2 =
      if result.is_a? Hash
        Myhash.to_lcc(result)
      else
        result
      end

    json =
      JSON.generate({
        "errors" => context[:errors],
        "result" => result2
      })

    to_api_triple(json)
  end

  def get_db
    "dummy_db"
  end

  def get(path, &block)
    @routing.add("GET", path, block)
  end

  def post(path, &block)
    @routing.add("POST", path, block)
  end

  def patch(path, &block)
    @routing.add("PATCH", path, block)
  end

  def delete(path, &block)
    @routing.add("DELETE", path, block)
  end

  def do_get(req, res)
    triple =
      if @routing.match?("GET", req.path)
        route, params = @routing.match("GET", req.path)
        @routing.dispatch(route, req, params)
      else
        send_file(req)
      end

    set_res(res, triple)
  end

  def do_post(req, res)
    print_stderr(
      [
        "req.path (#{req.path})",
        "method (POST)",
        "req.query (#{ req.query })"
      ].join("\n"),
      label: "do_post"
    )

    method = get_method(req)

    triple =
      if @routing.match?(method, req.path)
        route, params = @routing.match(method, req.path)
        @routing.dispatch(route, req, params)
      else
        bad_req()
      end

    set_res(res, triple)
  end
end

class Tdef
  KEYS = [
    :id, :name, :pname, :memo, :memo_sub, :ctime, :utime
  ]

  KEYS_OPT = [
    :group_id
  ]

  attr_reader(*KEYS)
  attr_reader(*KEYS_OPT)

  def initialize(h)
    KEYS.each do |k|
      unless h.key?(k)
        raise "key required (#{k})"
      end

      instance_variable_set("@#{k}", h[k])
    end

    KEYS_OPT.each do |k|
      if h.key?(k)
        instance_variable_set("@#{k}", h[k])
      end
    end
  end

  def self.from_plain(h)
    Tdef.new(h)
  end

  def to_plain
    (KEYS + KEYS_OPT)
      .map { |key|
        [
          key,
          send(key) # call getter
        ]
      }
      .to_h
  end
end

class Cdef
  KEYS = [
    :id, :name, :pname, :order, :memo, :memo_sub,
    :not_null, :pk,
    :type, :unsigned,
    :ctime, :utime
  ]

  KEYS_OPT = [
    :type_param,
    :uniq, :default_value,
    :fk_tdef_id, :fk_cdef_id
  ]

  attr_reader(*KEYS)
  attr_reader(*KEYS_OPT)

  def initialize(h)
    KEYS.each do |k|
      unless h.key?(k)
        raise "key required (#{k}) (#{h.inspect})"
      end

      instance_variable_set("@#{k}", h[k])
    end

    KEYS_OPT.each do |k|
      if h.key?(k)
        instance_variable_set("@#{k}", h[k])
      end
    end
  end

  def self.from_plain(h)
    Cdef.new(h)
  end

  def to_plain
    (KEYS + KEYS_OPT)
      .map { |key|
        [
          key,
          send(key) # call getter
        ]
      }
      .to_h
  end

  def merge(patch)
    Cdef.from_plain(to_plain().merge(patch))
  end
end

def set_res(res, triple)
  st, info, body = triple
  res.status = st

  if info.key?("Content-Type")
    res.content_type = info["Content-Type"]
  end

  res.body = body
end

def send_file(req)
  pub_dir = File.join(APP_DIR, "public")
  path = File.join(pub_dir, req.path)

  if File.exist?(path)
    [
      200,
      {},
      File.read(path)
    ]
  else
    [
      404,
      {},
      "not found #{req.path}"
    ]
  end
end

def get_method(req)
  req.query.fetch("_method", "POST")
end

def bad_req
  [
    400, # bad request
    { "Content-Type" => "text/plain" },
    "invalid req"
  ]
end

def app_path(tail)
  File.join(APP_DIR, tail)
end

def data_path(tail)
  File.join(DATA_DIR, tail)
end

def print_stderr(s, label: nil)
  $stderr.puts "  _____[ #{label} ]____"
  s.each_line { |line|
    $stderr.puts "  | " + line
  }
  $stderr.puts "  ~~~~~[ #{label} ]~~~~"
end

def puts_e(*args)
  lines = []
  args.each { |arg|
    lines << arg.to_s
  }
  print_stderr(
    lines.join("\n"),
    label: "puts_e"
  )
end

def p_e(*args)
  lines = []
  args.each { |arg|
    lines << arg.inspect
  }
  print_stderr(
    lines.join("\n"),
    label: "puts_e"
  )
end

def pp_e(*args)
  lines = []
  args.each { |arg|
    lines << arg.pretty_inspect
  }
  print_stderr(
    lines.join("\n"),
    label: "puts_e"
  )
end
