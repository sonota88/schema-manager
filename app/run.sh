#!/bin/bash

print_this_dir(){
  (
    cd "$(dirname "$0")"
    pwd
  )
}

cmd_up(){
  (
    cd "$(print_this_dir)"
    ruby server.rb
  )
}

cmd="$1"; shift
case $cmd in
  up )
    cmd_up
  ;; * )
    echo "invalid command" >&2
    exit 1
  ;;
esac
