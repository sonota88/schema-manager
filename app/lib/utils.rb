module Utils
  def puts_e(*args)
    args.each { |arg| $stderr.puts arg }
  end

  def p_e(*args)
    args.each { |arg| $stderr.puts arg.inspect }
  end

  def pp_e(*args)
    # args.each{|arg| $stderr.puts arg.pretty_inspect }
    $stderr.puts args.pretty_inspect
  end
end
