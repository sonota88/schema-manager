require_relative "dao_base"

# def get_data_dir
#   raise if $app_dir.nil?
#   File.join($app_dir, "../data")
# end

class TdefDao < DaoBase
  def _save(tdef, cdefs)
    path = tdef_path(tdef.id)
    json_file_save(
      path,
      {
        tdef: tdef.to_plain,
        cdefs: cdefs.map(&:to_plain)
      }
    )
  end

  def _get_tdef_ids_by_tag(tag_cond)
    pp_e [104, tag_cond]

    raise if tag_cond.nil?

    tag_els = tag_cond.split(",")
    sql = ""

    op, *rest = tag_els

    case op
    when "and"
      tag_names_csv = rest.map { |tag| "'#{tag}'" }.join(", ")

      sql = "
        select table_id
        from (
            select table_id, count(1) as cnt
            from rel_table_tag
            where tag_id in (
              select tag_id
              from table_tag
              where tag_name in (#{tag_names_csv})
            )
            group by table_id
          )
        where cnt = #{rest.size}
      "
    when "or"
      tag_names_csv = rest.map { |tag| "'#{tag}'" }.join(", ")

      sql = "
        select distinct table_id
        from rel_table_tag
        where tag_id in (
          select tag_id
          from table_tag
          where tag_name in (#{tag_names_csv})
        )
      "
    else
      tag = op

      sql = "
        select table_id
        from rel_table_tag
        where tag_id in (
          select tag_id
          from table_tag
          where tag_name = '#{tag}'
        )
      "
    end

    puts_e 129, sql

    recs = _execute_v2(sql)
    pp_e [100, recs, recs.map { |rec| rec[:table_id] }]
    tdef_ids = recs.map { |rec| rec[:table_id] }

    tdef_ids
  end

  def _get_tags(tdefs)
    tdef_ids = tdefs.map { |tdef| tdef[:table_id] }

    sql = "
      select t1.table_id as table_id
      , t2.tag_name as tag_name
      from (
          select *
          from rel_table_tag
          where table_id in (#{ tdef_ids.join(", ") })
        ) t1
        inner join table_tag t2
          on t2.tag_id = t1.tag_id
    "
    recs = _execute_v2(sql)

    recs
  end

  def valid_word?(word)
    if word.match?(/[;,'\\]/)
      false
    else
      true
    end
  end

  def _build_cond_word(word_csv)
    kws = word_csv.split(",")
    kws.each { |kw|
      raise "invalid keyword" unless valid_word?(kw)
    }

    tree = [:or]
    tree +=
      %w[name pname].map { |col|
        # いまのところ AND 検索のみ対応
        [:and] + kws.map { |kw| "t1.#{col} like '%#{kw}%'" }
      }
    pp_e 246, tree

    SqlCond.render(tree)
  end

  def _scond_match?(scond, tdef)
    if scond[:g]
      if tdef.group_id == scond[:g]
        # match
      else
        return false
      end
    end

    true
  end

  def search(scond)
    paths = Dir.glob(data_path("tdefs/*")).to_a

    tdefs = []

    paths.each { |path|
      data = json_file_load(path, sym_key: true)
      tdef = Tdef.from_plain(data[:tdef])

      if _scond_match?(scond, tdef)
        tdefs << tdef
      end
    }

    tdefs
  end

  def search_for_fk(kw)
    words = kw.split(/ +/)
    paths = Dir.glob(data_path("tdefs/*.json")).to_a
    list = []

    # TODO 配列の場合に sym_key: true が効かない
    groups = json_file_load(data_path("groups.json"), sym_key: false)
    group_map = {}
    groups.each { |group|
      group_map[group["id"]] = group["name"]
    }

    paths.each do |path|
      data = json_file_load(path, sym_key: true)
      tdef = Tdef.from_plain(data[:tdef])

      list +=
        data[:cdefs]
          .map { |it| Cdef.from_plain(it) }
          .select { |cdef|
            s = [tdef.pname, cdef.pname].join(" ")
            words.all? { |w| s.include?(w) }
          }
          .map { |cdef|
            group_id = -1
            group_name = "?"
            if tdef.group_id
              group_id = tdef.group_id
              group_name = group_map[group_id]
            end

            {
              tdef_id: tdef.id,
              tdef_pname: tdef.pname,
              id: cdef.id,
              pname: cdef.pname,
              group_id: group_id,
              group_name: group_name
            }
          }
    end

    list
  end

  def get(id)
    path = tdef_path(id)
    data = json_file_load(path, sym_key: true)
    Tdef.from_plain(data[:tdef])
  end

  def valid_tag?(tag)
    if tag.match?(/[;,'\\]/)
      false
    else
      true
    end
  end

  def _insert_tag(tag)
    sql = "
      select max(tag_id) as max_id from table_tag
    "
    xs = _execute_v2(sql)

    max_id = xs[0][:max_id]
    new_id = max_id + 1

    unless valid_tag?(tag)
      raise "invalid char (; , ' \\) in tag name"
    end

    _insert(
      "table_tag", {
        :tag_id   => new_id,
        :tag_name => tag.strip
      }
    )

    new_id
  end

  def _update_tags(tdef)
    pp_e [250, tdef.id, tdef.tags]

    tag_ids = []

    tdef.tags.each do |tag|
      unless valid_tag?(tag)
        raise "invalid char (; , ' \\) in tag name"
      end
      sql = "
        select tag_id from table_tag
        where tag_name = '#{tag}'
      "
      xs = _execute_v2(sql)
      if xs.empty?
        new_id = _insert_tag(tag)
        tag_ids << new_id
      else
        tag_ids << xs[0][:tag_id]
      end
    end

    sql = "
      delete from rel_table_tag
      where table_id = #{tdef.id}
    "
    _execute(sql, [])

    tag_ids.each do |tag_id|
      _insert(
        "rel_table_tag", {
          :table_id => tdef.id,
          :tag_id   => tag_id
        }
      )
    end
  end

  # グループ内でユニークであること
  def _assert_uniq_pname(group_id, tdef_id, pname)
    sql = "
      select 1
      from table_def
      where group_id = ?
        and tdef_id <> ?
        and pname = ?
    "

    recs = _execute_v2(
      sql,
      [group_id, tdef_id, pname]
    )

    unless recs.empty?
      raise "pname already exists"
    end
  end

  def update(tdef)
    # pp_e [377, tdef]
    path = tdef_path(tdef.id)
    data = json_file_load(path)

    data["tdef"] = tdef.to_plain
    # pp_e [380, data]

    # TODO validate: pname not null
    # TODO validate: pname uniq

    json_file_save(path, data)
  end

  def insert(tdef)
    # data = {
    #   tdef: tdef.to_plain,
    #   cdefs: []
    # }

    _save(tdef, [])
  end

  def create
    new_id = max_id() + 1
    now = Time.now.strftime("%F_%T")

    tdef = Tdef.new(
      {
        id: new_id,
        name: "",
        pname: "table_#{new_id}",
        memo: "",
        memo_sub: "",
        ctime: now,
        utime: now
      }
    )

    insert(tdef)
  end

  def max_id
    Dir.glob(DATA_DIR + "/tdefs/*").to_a
      .map { |path|
        File.basename(path, ".json")
          .sub(/^0+/, "")
          .to_i
      }
      .max
  end

  def get_using_tdefs(id)
    paths = Dir.glob(data_path("tdefs/*.json")).to_a

    list = []

    paths.each do |path|
      data = json_file_load(path, sym_key: true)
      tdef = Tdef.from_plain(data[:tdef])

      if (
        data[:cdefs]
          .map { |it| Cdef.from_plain(it) }
          .any? { |cdef| cdef.fk_tdef_id == id }
      )
        list <<
          {
            id: tdef.id,
            pname: tdef.pname
          }
      end
    end

    list
  end
end

class TagDao < DaoBase
  def search(sql_cond)
    # targets = []
    sql = "
      select tag_id, tag_name
      from table_tag
      where tag_id in (
        select tag_id
        from rel_table_tag
        where 1=1
          and #{sql_cond}
      )
    "
    recs = _execute_v2(sql)

    recs
  end
end

class CdefDao < DaoBase
  def get(tdef_id, cdef_id)
    path = tdef_path(tdef_id)

    data = json_file_load(path, sym_key: true)

    cdef =
      Cdef.from_plain(
        data[:cdefs]
          .find { |cdef| cdef[:id] == cdef_id }
      )

    cdef
  end

  def get_for_table(tdef_id)
    path = tdef_path(tdef_id)
    data = json_file_load(path)

    data["cdefs"]
  end

  def max_id(tdef_id)
    path = tdef_path(tdef_id)
    data = json_file_load(path, sym_key: true)
    ids = data[:cdefs].map { |cdef| cdef[:id] }

    if ids.empty?
      0
    else
      ids.max
    end
  end

  def _max_order(tdef_id)
    path = tdef_path(tdef_id)
    data = json_file_load(path, sym_key: true)
    orders = data[:cdefs].map { |cdef| cdef[:order] }

    if orders.empty?
      0
    else
      orders.max
    end
  end

  def insert(tdef_id, cdef_id, cdef = nil)
    now = Time.now.strftime("%F_%T")
    order = _max_order(tdef_id) + 10

    plain =
      if cdef
        cdef.to_plain.merge({ id: cdef_id })
      else
        {
          id: cdef_id,
          name: "",
          pname: "c#{cdef_id}",
          memo: "",
          memo_sub: "",
          order: order,
          not_null: false,
          pk: false,
          type: nil,
          unsigned: nil,
          ctime: now,
          utime: now
        }
      end
    new_cdef = Cdef.new(plain)

    path = tdef_path(tdef_id)
    data = json_file_load(path, sym_key: true)

    data[:cdefs] << new_cdef.to_plain

    data[:cdefs] = _adjust_order(data[:cdefs])

    json_file_save(path, data)
  end

  # テーブル内でユニークであること
  def _assert_uniq_pname(tdef_id, cdef_id, pname)
    sql = "
      select 1
      from col_def
      where tdef_id = ?
        and cdef_id <> ?
        and pname = ?
    "

    recs = _execute_v2(
      sql,
      [tdef_id, cdef_id, pname]
    )

    unless recs.empty?
      raise "pname already exists"
    end
  end

  def _adjust_order(cdefs)
    cdefs
      .sort_by { |cdef| cdef[:order] }
      .each_with_index { |cdef, i|
        cdef[:order] = (i + 1) * 10
      }

    cdefs
  end

  # TODO Move to utils
  def is_blank?(s)
    return true if s.nil?
    s.strip.empty?
  end

  def update(tdef_id, cdef_id, cdef)
    path = tdef_path(tdef_id)
    data = json_file_load(path, sym_key: true)

    data[:cdefs] =
      data[:cdefs]
        .map { |_cdef|
          if _cdef[:id] == cdef_id
            cdef.to_plain()
          else
            _cdef
          end
        }

    data[:cdefs] =
      _adjust_order(data[:cdefs])

    json_file_save(path, data)
  end

  def delete(tdef_id, cdef_id)
    path = tdef_path(tdef_id)
    data = json_file_load(path, sym_key: true)

    data[:cdefs] =
      data[:cdefs]
        .reject { |cdef| cdef[:id] == cdef_id }

    json_file_save(path, data)
  end
end

class GroupDao < DaoBase
  def search
    recs = _execute_v2(
      "select * from table_group"
    )

    recs.map { |rec|
      Myhash.map_key2(
        rec,
        {
          :group_id => :id
        }
      )
    }
  end

  def get(id)
    # targets = []
    sql = "
      select
        name
      from table_group
      where group_id = ?
    "
    params = [id]
    x = _execute_v2(sql, params)[0]

    x
  end
end

################################
# app.rb

def to_lcc s
  words = s.split("_")
  words[0] + words[1..-1].map { |word|
    word.capitalize
  }.join("")
end
