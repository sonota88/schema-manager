# require "anbt-sql-formatter/formatter"

require "json"

require_relative "../utils"

class DaoBase
  include Utils

  def initialize(db)
    @db = db
  end

  def _dedent text
    ls = text.split("\n")
    # p_e ls

    ls2 =
      if ls[0] == ""
        ls[1..-1]
      else
        ls
      end
    # p_e ls2

    ls3 =
      if ls2[-1].match?(/^ *$/)
        ls2[0..-2]
      else
        ls2
      end
    # p_e ls3

    sps = ls3.select { |line|
      /^ *$/ !~ line # 空行は除く
    }.map { |line|
      /^( *)/ =~ line
      $1.size
    }
    # p_e sps

    min = sps[0]
    sps.each { |n|
      min = n if n < min
    }

    head = " " * min
    ret = ls3.map { |x|
      x.sub(/^#{head}/, "")
    }.join("\n")
    # puts_e ">>" + ret + "<<"

    ret
  end

  def _dedent2(t, last_newline = true)
    t.gsub(/^\s+:/, "")
  end

  def _indent(text, n)
    head = " " * n
    text.split("\n").map { |line|
      head + line
    }.join("\n")
  end

  def sqlite3_row2obj(row)
    Myhash.new(row)
      .filter { |k, v| k.is_a? String }
      .to_plain
  end

  def sqlite3_row2obj_v2(row)
    Myhash.new(row)
      .filter { |k, v| k.is_a? String }
      .to_sym_key
      .to_plain
  end

  def _execute(sql, params = [])
    _sql = _indent(_dedent(sql), 2)
    puts_e "-->>"
    puts_e fmt_sql(sql)
    puts_e "<<--"
    puts_e "sql (\n" + _sql + "\n)"
    puts_e "params (\n" + params.inspect + "\n)"
    @db.execute(sql, params) { |row|
      yield(row)
    }
  end

  def _execute_v2(sql, params = [])
    recs = []
    _execute(sql, params) do |row|
      recs << sqlite3_row2obj_v2(row)
    end
    recs
  end

  def _insert(table, map)
    puts_e "insert params (\n" + map.inspect + "\n)"

    keys = map.keys
    values = keys.map { |key| map[key] }

    cols_csv = keys.join(", ")
    placeholders_csv = Array.new(keys.size, "?").join(", ")

    sql = "
      INSERT into #{table}
      (#{cols_csv})
      values
      (#{placeholders_csv})
    "

    _execute(sql, values)
  end

  def now_ts
    Time.now.strftime("%F_%T")
  end

  def assert_not_blank(str, msg = "")
    if str.nil? || /^\s*$/ =~ str
      raise "should not be blank (#{msg}) (#{str.inspect})"
    end
  end

  # def fmt_sql(sql)
  #   rule = AnbtSql::Rule.new
  #   formatter = AnbtSql::Formatter.new(rule)
  #   formatter.format(sql.dup)
  # end

  def tdef_path(tdef_id)
    data_path(format("tdefs/%04d.json", tdef_id))
  end

  def json_file_load(path, sym_key: false)
    data = JSON.parse(File.read(path))

    if sym_key
      Myhash.new(data)
        .to_sym_key
        .to_plain
    else
      data
    end
  end

  def json_file_save(path, data)
    File.open(path, "wb") { |f|
      f.write JSON.pretty_generate(data)
    }
  end
end
