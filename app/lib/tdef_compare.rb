=begin
{
  cdefs: [
    {
      pname: "id",
      type: "int",
      memo: "",
    },
  ]
}
=end

class TdefCompare
  def self.convert_other(data)
    # Customize here

    data[:cdefs].each { |cdef|
      cdef[:type]&.downcase!

      cdef[:unsigned] = cdef[:attr]&.strip == "UNSIGNED"
      cdef.delete(:attr)

      [:pk, :not_null].each { |k|
        cdef[k] =
          case cdef[k]
          when "true" then true
          when "false" then false
          when "" then false
          else
            cdef[k]
          end
      }
    }

    data
  end

  def self.write_json(path, data)
    File.open(path, "wb") { |f|
      f.puts(
        JSON.pretty_generate(data)
      )
    }
  end

  def self.compare_colnames(data_self, data_other)
    tempfile_s = "/tmp/tim_diff_cols_self.json"
    tempfile_o = "/tmp/tim_diff_cols_other.json"

    write_json(
      tempfile_s,
      data_self[:cdefs].map { |cdef| cdef[:pname] }
    )
    write_json(
      tempfile_o,
      data_other[:cdefs].map { |cdef| cdef[:pname] }
    )

    cmd = %(diff -u -w "#{tempfile_o}" "#{tempfile_s}")
    `#{cmd}`
  end

  def self._to_cdef_diff_text(cdef)
    lines = []

    cdef
      .to_a
      .reject { |k, _| k == :order }
      .sort_by { |k, _| k }
      .each { |k, v|
        lines << "  #{k.inspect}: #{v.inspect}"
      }

    lines
      .map { |line| line + "\n" }
      .join()
  end

  def self._to_cdefs_diff_text(data)
    lines = []

    data[:cdefs].each { |cdef|
      lines << "{"
      cdef
        .to_a
        .reject { |k, _| k == :order }
        .sort_by { |k, _| k }
        .each { |k, v|
          lines << "  #{k.inspect}: #{v.inspect}"
        }
      lines << "}"
    }

    lines
      .map { |line| line + "\n" }
      .join()
  end

  def self.col_and_order_list(data_self)
    data_self[:cdefs]
      .map { |cdef|
        [
          cdef[:order],
          cdef[:pname]
        ].join(", ")
      }
      .join("\n")
  end

  def self.diff_whole(data_self, data_other)
    # temp file
    tempfile_s = "/tmp/tim_diff_self.json"
    tempfile_o = "/tmp/tim_diff_other.json"

    File.open(tempfile_s, "wb") { |f| f.puts _to_cdefs_diff_text(data_self) }
    File.open(tempfile_o, "wb") { |f| f.puts _to_cdefs_diff_text(data_other) }

    cmd = %(diff -u -w "#{tempfile_o}" "#{tempfile_s}")
    diff = `#{cmd}`
  end

  def self.diff_cols(data_self, data_other)
    # temp file
    tempfile_s = "/tmp/tim_diff_self.json"
    tempfile_o = "/tmp/tim_diff_other.json"

    result = []

    self_order_max =
      if data_self[:cdefs].empty?
        10
      else
        data_self[:cdefs].map { |cdef| cdef[:order] }.max
      end

    data_other[:cdefs].each_with_index { |cdef_o, i|
      cdef_s = data_self[:cdefs].find{|it| it[:pname] == cdef_o[:pname] }

      File.open(tempfile_o, "wb") { |f| f.print "" }
      File.open(tempfile_s, "wb") { |f| f.print "" }

      File.open(tempfile_o, "wb") { |f| f.puts _to_cdef_diff_text(cdef_o) }

      File.open(tempfile_s, "wb") { |f|
        f.puts(
          if cdef_s
            _to_cdef_diff_text(cdef_s)
          else
            ""
          end
        )
      }
      cmd = %(diff -u -w "#{tempfile_o}" "#{tempfile_s}")
      diff = `#{cmd}`

      order_ratio = i.to_f / data_other[:cdefs].size
      order = (order_ratio * self_order_max).floor

      result << {
        cdef: cdef_o,
        order: order,
        diff: diff
      }
    }

    result
  end

  def self.compare(data_self, data_other_raw)
    pp_e ["tdef_compare 25", data_self, data_other_raw]

    data_other = convert_other(data_other_raw)

    diff_all =
      [
        compare_colnames(data_self, data_other),
        "========",
        col_and_order_list(data_self),
        "========",
        diff_whole(data_self, data_other),
      ].join("\n")

    [
      diff_all,
      diff_cols(data_self, data_other),
      data_other
    ]
  end
end
