class TreeBuilder {
  static _appendChild(parent, child){
    if (
      ["string", "number", "boolean"].includes(typeof child)
    ) {
      parent.appendChild(document.createTextNode(child));
    } else {
      parent.appendChild(child);
    }
  }

  static _build(tag, attrs, ...children){
    const el = document.createElement(tag);
    const append = TreeBuilder._appendChild;

    for (const k of Object.keys(attrs || {})) {
      const v = attrs[k];
      if (/^on(click|change|keydown|input)$/.test(k)) {
        const eventName = k.substring(2);
        el.addEventListener(eventName, v, false);
      } else if (
        k === "selected"
        || k === "checked"
      ) {
        if (v && (v !== TreeBuilder.NO_ATTR)) {
          el.setAttribute(k, k);
        }
      } else if (k === "style") {
        for (const sk of Object.keys(v)) {
          const sv = v[sk];
          el.style[sk] = sv;
        }
      } else {
        el.setAttribute(k, v);
      }
    }

    children.forEach((child)=>{
      if(
        child == null
      ){
        ;
      } else {
        if (
          ["string", "number", "boolean"].includes(typeof child)
        ) {
          append(el, document.createTextNode(child));
        } else if (Array.isArray(child)) {
          child.forEach(_child => append(el, _child));
        } else {
          append(el, child);
        }
      }
    });

    return el;
  }

  static build(fn){
    return fn(TreeBuilder.h);
  }

  static bool2attr(val){
    return val ? true : TreeBuilder.NO_ATTR;
  }
}

TreeBuilder.h = TreeBuilder._build.bind(TreeBuilder);
TreeBuilder.NO_ATTR = "__NO_ATTR__";
