const h = TreeBuilder.h;

class SearchCond {
  static serializeOrders(orders){
    return orders
      .map(order => order.k + "=" + order.v)
      .join(",");
  }

  static buildUrl(params){
    const usp = new URLSearchParams();

    if (params.page != null) {
      usp.append("p", params.page);
    }
    if (params.orders.length > 0) {
      usp.append("o", SearchCond.serializeOrders(params.orders));
    }
    if (! __g.isBlank(params.word)) {
      usp.append("q", params.word);
    }
    if (! __g.isBlank(params.tag)) {
      usp.append("t", params.tag);
    }
    if (! __g.isBlank(params.group)) {
      usp.append("g", params.group);
    }

    let href = __g.appPath("/tdefs/");
    if (usp.toString().length >= 1) {
      href += "?" + usp.toString()
    }

    return href;
  }

  static buildUrl_v2(){
    const sc = __p.state.searchCond;
    return this.buildUrl({
      page:   sc.page
      ,orders: sc.orders_v2
      ,word:   sc.word
      ,tag:    sc.tag
      ,group:  sc.group
    });
  }

  static getTheadOrder(key){
    const orders = __p.state.searchCond.orders_v2;
    const order = orders.find(ord => ord.k === key);
    if (order != null) {
      return {
        asc: "▲"
        ,desc: "▼"
      }[order.v];
    } else {
      return "";
    }
  }

}

class Pagination {
  static linkToFirst(scond){
    return SearchCond.buildUrl({
      page: null
      ,word: scond.word
      ,tag: scond.tag
      ,orders: scond.orders_v2
      ,group: scond.group
    });
  }

  static linkToPrev(scond){
    return SearchCond.buildUrl({
      page: scond.pagePrev
      ,word: scond.word
      ,tag: scond.tag
      ,orders: scond.orders_v2
      ,group: scond.group
    });
  }

  static linkToNext(scond){
    return SearchCond.buildUrl({
      page: scond.pageNext
      ,word: scond.word
      ,tag: scond.tag
      ,orders: scond.orders_v2
      ,group: scond.group
    });
  }

  static render(scond){
    return (
      h("div", { "class": "pagination" }
      , h("table", {}
        , h("tr", {}
          , h("td", {}
            , h("a", { href: this.linkToFirst(scond) }, "first")
            )
          , h("td", {}
            , scond.pagePrev
              ? h("a", { href: this.linkToPrev(scond) }, "prev")
              : "-"
            )
          , h("td", {}
            , scond.page
            )
          , h("td", {}
            , scond.pageNext
              ? h("a", { href: this.linkToNext(scond) }, "next")
              : "-"
            )
          )
        )
      )
    );
  }
}


class TagLink {
  static render(tag){
    return (
      h("a", {
          "class": "tag_link"
        , "href": __g.appPath(`/tdefs/?t=${tag}`)
        }
      , tag
      )
    );
  }
}

class TableDefRow {
  static render(tdef){

    let memo;
    if (tdef.memo.length >= 50) {
      memo = tdef.memo.substring(0, 50) + " …";
    } else {
      memo = tdef.memo;
    }

    return (
      h("tr", {}
      , h("td", {}
        , h("div", { "class": "telomere telomere_" + __p.toTelomereClass(tdef.utime) })
        )
      , h("td", {}, tdef.id)
      , h("td", {}
        , h("a", { href: `./${tdef.id}` }, tdef.name)
        )
      , h("td", {}
        , h("a", { href: `./${tdef.id}` }, tdef.pname)
        )
      , h("td", {}
        , memo
        )
      , h("td", {}
        , "TODO"
        )
      , h("td", {}
        , h("a", { href: __g.appPath(`/tables/?g=${tdef.groupId}`) }
          , __p.getGroupName(tdef.groupId)
          )
        )
      , h("td", {}
        , (tdef.tags || []).map(tag => TagLink.render(tag))
        )
      , h("td", {}, tdef.utime)
      )
    );
  }
}

class View {
  static render(state){
    return (
      h("div", {}
      , h("h1", {}, "テーブルの一覧")
      , h("button", {
            onclick: ()=>{ __p.onclick_createNewTdef(); }
          }
        , "新規作成"
        )

      /*
      , h("hr")
      , h("input", {
            name: "input_word_search"
          , style: { width: "16rem" }
          , value: __p.wordCondForInput()
          , onchange: ()=>{ __p.onchange_word(); }
          , onkeydown: (ev)=>{ __p.onkeydown_word(ev); }
          , title: "スペース区切りで AND検索"
          }
        )
      , h("button", {
            onclick: ()=>{ __p.onclick_wordSearch(); }
          }, "検索"
        )

      , h("input", {
            name: "input_tag_search"
          , style: { width: "16rem" }
          , value: __p.tagcondForInput()
          , title: '"and tag1 tag2" で AND検索'
          , onchange: ()=>{ __p.onchange_tag(); }
          , onkeydown: (ev)=>{ __p.onkeydown_tag(ev); }
          }
        )
      , h("button", {
            onclick: ()=>{ __p.onclick_tagSearch(); }
          }
        , "タグ検索"
        )

      , h("br")
      , h("a", { href: __g.appPath("/tdefs/") }, "検索条件をリセット")

      , Pagination.render(state.searchCond)

      , h("a", { href: __g.appPath("/tdefs/?o=utime=desc") }, "utime desc")
      */

      , h("table", { "class": "hover_hl, tdef_table" }
        , h("tr", {}
          , h("th", {}, "") // telomere
          , h("th", {}, "id")
          , h("th", {
                style: { "min-width": "6rem" }
              }
            , "name"
            )
          , h("th", {
                onclick: ()=>{ __p.onclick_pnameThead(); }
              , "class": "clickable_thead"
              }
            , "pname " + SearchCond.getTheadOrder("pname")
            )
          , h("th", {}, "memo")
          , h("th", {}, "memo sub")
          , h("th", {}, "group")
          , h("th", {}, "tags")
          , h("th", {}, "utime")
        )
        , state.tables.map(tabledef => TableDefRow.render(tabledef))
        )

      // , Pagination.render(state.searchCond)
      )
    );
  }
}

class Page {
  constructor() {
    this.state = {};
  }

  getTitle() { return "テーブル定義の一覧"; }

  updateView() {
  }

  render() {
    __g.replaceEl("#main", View.render(this.state));
    this.updateView();
  }

  // view helpers

  toTelomereClass(dt) {
    const seconds = (new Date() - Date.parse(dt.replace("_", " "))) / 1000.0;
    const mins = seconds / 60.0;
    const hours = mins / 60.0;
    const days = hours / 24.0;
    if        (mins  <=   5) { return "5m";
    } else if (mins  <=  30) { return "30m";
    } else if (hours <=   1) { return "1h";
    } else if (hours <=   4) { return "4h";
    } else if (days  <=   1) { return "1d";
    } else if (days  <=   7) { return "1w";
    } else if (days  <=  31) { return "1mon";
    } else if (days  <= 365) { return "1y";
    } else { return "gt_1y";
    }
  }

  wordCondForInput(){
    const cond = this.state.searchCond.word;
    if (cond == null) {
      return "";
    }

    return cond.split(",").join(" ");
  }

  tagcondForInput(){
    const tagCond = this.state.searchCond.tag;
    if (tagCond == null) {
      return "";
    }

    return tagCond.split(",").join(" ");
  }

  onchange_word(){
    const cond = $("[name=input_word_search]").val().split(/ +/).join(",");
    if (__g.isBlank(cond)) {
      __p.state.searchCond.word = null;
    } else {
      __p.state.searchCond.word = cond;
    }
    this.updateView();
  }

  onkeydown_word(ev){
    if (ev.key === 'Enter') {
      this.onchange_word();
      __p.state.searchCond.page = null;
      location.href = SearchCond.buildUrl_v2();
    }
  }

  onclick_wordSearch(ev){
    __p.state.searchCond.page = null;
    location.href = SearchCond.buildUrl_v2();
  }

  onchange_tag(){
    const tagCond = $("[name=input_tag_search]").val().split(/ +/).join(",");
    if (__g.isBlank(tagCond)) {
      __p.state.searchCond.tag = null;
    } else {
      __p.state.searchCond.tag = tagCond;
    }
    this.updateView();
  }

  onkeydown_tag(ev){
    if (ev.key === 'Enter') {
      this.onchange_tag();
      __p.state.searchCond.page = null;
      location.href = SearchCond.buildUrl_v2();
    }
  }

  onclick_tagSearch(ev){
    __p.state.searchCond.page = null;
    location.href = SearchCond.buildUrl_v2();
  }

  onclick_pnameThead(){
    this.state.searchCond.page = null;

    const orders = this.state.searchCond.orders_v2;
    const pnameOrder = orders.find(ord => ord.k === "pname");

    const newOrders = orders.filter(ord => ord.k !== "pname");

    if (pnameOrder == null) {
      newOrders.unshift({ k: "pname", v: "asc" });
    } else {
      if (pnameOrder.v === "asc") {
        newOrders.unshift({ k: "pname", v: "desc" });
      }else if (pnameOrder.v === "desc") {
        ;
      } else {
        throw new Error("must not happen");
      }
    }

    this.state.searchCond.orders_v2 = newOrders;

    location.href = SearchCond.buildUrl_v2();
  }

  buildParams() {
    const params = {};
    const url = new URL(location.href);

    const q = url.searchParams.get("q");
    if (q) { params.q = q; }

    const tag = url.searchParams.get("t");
    if (tag) { params.t = tag; }

    const page = url.searchParams.get("p");
    if (page) { params.p = _parseInt(page); }

    const groupIdStr = url.searchParams.get("g");
    if (groupIdStr) { params.g = _parseInt(groupIdStr); }

    return params;
  }

  prepareState() {
    // 暫定
    this.state.searchCond.orders_v2 = this.state.searchCond.orders;

    this.state.searchCond.orders =
      this.state.searchCond.orders
      .map(ord => `${ord.k}=${ord.v}`)
      .join(",");
  }

  getGroupName(groupId) {
    const group = this.state.groups.find(g => g.id === groupId);
    if (group) {
      return group.name;
    } else {
      return "?";
    }
  }

  init(){
    puts("-->> init", __g.getApiUrl());

    const params = this.buildParams();

    __g.api("get", __g.getApiUrl(), params, (result)=>{
      puts(result);
      this.state = result;

      this.prepareState();

      this.render();
      __g.unguard();
    }, (errors)=>{
      __g.unguard();
      __g.printApiErrors(errors);
    });
  }

  onclick_createNewTdef() {
    const doCreate = confirm("作成しますか？");
    if (!doCreate) { return; }

    __g.api(
      "post", `/api/tables/`, {},
      result => {
        location.reload();
      },
      errors => {
        alert("See console");
        puts(434, errors);
      },
    );
  }
}

__g.ready(new Page());
