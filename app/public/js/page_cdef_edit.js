const h = TreeBuilder.h;

class Cdef {
  constructor(pname, unsigned, type, typeParam) {
    this.pname = pname;
    this.unsigned = unsigned;
    this.type = type;
    this.typeParam = typeParam;
  }

  static fromPlain(plain) {
    return new Cdef(
      plain.pname,
      plain.unsigned,
      plain.type,
      plain.typeParam
    );
  }
}

class FkSearch {
  static render(state){
    return (
      h("div", {
          style: {
            border: "solid 0.1rem #ddd"
          , "border-radius": "0.3rem"
          , background: "#f8f8f8"
          , margin: "1rem 0"
          }
        }
      , "fk search: "
      , h("input", {
            name: "input_fk_search"
          , value: state.kw
          , title: "table または table col"
          }
        )
      , h("button", {
            onclick: (ev)=>{ __p.oninput_fkKeyWord(); }
          }
        , "search"
        )
      , h("ul", {}
        , state.cdefs.map((cdef)=>
            h("li", {}
            , h("button", {
                  onclick: ()=>{
                    __p.onclick_selectFk(cdef.tdefId , cdef.id);
                  }
                }
              , `${ cdef.groupName }: `
              , `(${ cdef.tdefId }-${ cdef.id })`
              , ` ${ cdef.tdefPname } . ${ cdef.pname }`
              )
            )
          )
        )
      )
    );
  }
}

class PkInput {
  static render(val) {
    return MyToggleCheckbox.render(
      val
    , "pk"
    , (ev)=>{ __p.onchange_pk(ev); }
    );
  }
}

class RequiredInput {
  static render(val) {
    return MyToggleCheckbox.render(
      val
    , "not null"
    , (ev)=>{ __p.onchange_required(ev); }
    );
  }
}

class TypeSelection {
  static render(opts, cdef_v2) {
    let cls = "label_hl";
    if (cdef_v2.unsigned) {
      cls += " label_selected";
    }

    const typeButton = (type) =>
      h("button", {
          onclick: ()=>{ __p.onclick_typeBtn(type); }
        }
      , type
      );

    return (
      h("div", {}

      , h("input", {
            name: "custom_type"
          , value: cdef_v2.type
          , onchange: ()=>{ __p.onchange_customType(); }
          }
        )

      , MyCheckbox.render(
          "unsigned"
        , (cdef_v2.unsigned == null ? false : cdef_v2.unsigned) // 未設定の場合はチェックなし
        , {
            name: "chk_optional_type_unsigned"
          , onchange: ()=>{ __p.onchange_type(); }
          }
        )

      , h("div", { "class": "box box_indent" }
        , "MySQL"
        , typeButton("tinyint")
        , typeButton("smallint")
        , typeButton("mediumint")
        , typeButton("int")
        , typeButton("bigint")
        , typeButton("decimal")
        , typeButton("tinytext")
        , typeButton("varchar")
        , typeButton("text")
        , typeButton("enum")
        , typeButton("timestamp")
        , typeButton("time")
        , typeButton("datetime")
        , typeButton("date")
        , typeButton("tinyblob")
        )

      , h("div", { "class": "box box_indent" }
        , "type param: "
        , h("input", {
              name: "type_param"
            , value: cdef_v2.typeParam || ""
            , onchange: ()=>{ __p.onchange_type_param(); }
            }
          )
        )
      )
    );
  }
}

class View {
  static render(state){
    const cdef = state.cdef;
    const cdef_v2 = Cdef.fromPlain(state.cdef);

    return ([
      h("h1", {}
      , "カラムの編集"
      )
    , h("a", { href: `${ __g.appPath('/tables/' + __p.getTdefId()) }` }, "戻る")

    , h("hr")

    , "tdef_id"
    , h("input", { name: "tdef_id", value: state.tdef.id, type: "number", disabled: null })

    , " / cdef_id"
    , h("input", { name: "cdef_id", value: cdef.id, type: "number", disabled: null })

    , " / order"
    , h("input", { name: "order", value: cdef.order, type: "number"
        , onchange: ()=>{ __p.onchange_order(); }
        }
      )

    , h("br")

    , "pname"
    , h("input", { name: "pname", value: cdef_v2.pname || ""
        , onchange: ()=>{ __p.onchange_pname(); }
        , "class": "input_name"
        }
      )
    , " / name"
    , h("input", {
          name: "name", value: cdef.name || ""
        , onchange: ()=>{ __p.onchange_name(); }
        , "class": "input_name"
        })


    , h("hr")

    , "fk tdef id"
    , h("input", {
          name: "fk_tdef_id", value: cdef.fkTdefId || ""
        , type: "number"
        , onchange: ()=>{ __p.onchange_fkTdefId(); }
        }
      )
    , " / fk cdef id"
    , h("input", {
          name: "fk_cdef_id", value: cdef.fkCdefId || ""
        , type: "number"
        , onchange: ()=>{ __p.onchange_fkCdefId(); }
        }
      )

    , h("div", { "class": "box_indent" }
      , FkSearch.render(state.fkSearch)
      )

    , h("hr")

    , h("br")
    , PkInput.render(state.cdef.pk)
    , " / ", RequiredInput.render(state.cdef.notNull)
    
    , h("div", {}
      , "unique: "
      , h("input", {
            name: "optional_uniq"
          , value: state.cdef.uniq || ""
          , onchange: ()=>{ __p.onchange_uniq(); }
          }
        )

      , " / "

      , "default: "
      , h("input", {
          value: state.cdef.defaultValue || ""
        , onchange: (ev)=>{
            __p.onchange_defaultValue(ev);
          }
        })
      )

    , h("h2", {}, "型")
    , TypeSelection.render(state.cdef.opts, cdef_v2)

    , h("h2", {}, "よく使うもの（コピペ用）")
    , h("div", { "class": "box_indent" }
      , h("input", { value: "CURRENT_TIMESTAMP"
        , onclick: (ev)=>{ __g.selectInputText(ev); }
        })
      , h("input", { value: "auto_increment=true"
        , onclick: (ev)=>{ __g.selectInputText(ev); }
        })
      )

    , h("h2", {}, "メモ")

    , h("textarea", { name: "memo"
          , onchange: ()=>{ __p.onchange_memo(); }
          , style: { width: "100%", height: "10rem" }
        }
      , cdef.memo
      )

    , h("h2", {}, "メモ（自分用）")

    , h("textarea", {
            name: "memo_sub"
          , onchange: ()=>{ __p.onchange_memoSub(); }
          , style: { width: "100%", height: "10rem" }
        }
      , cdef.memoSub
      )

    , h("div", {
          style: {
            background: "#f0f0f0"
          , position: "fixed"
          , bottom: "0"
          , padding: "0.5rem"
          , "box-shadow": "0 0 0.3rem rgba(0,0,0, 0.2)"
          , border: "solid 0.1rem #ccc"
          }
        }
      , h("a", { href: `${ __g.appPath('/tables/' + __p.getTdefId()) }` }, "戻る")
      , " / "
      , h("button", {
            id: "submit"
          , onclick: (ev)=>{ __p.submit(ev); }
          }
        , "保存"
        )
      )

    , h("hr")
    , h("div", { id: "debug_box_container" })
    , h("div", { style: { height: "4rem" } }) // スペース調整用
    ]);
  }
}

class Page {
  constructor(){
    this.state = {
      cdef: {
        name: "", pname: ""
        ,order: -1
        ,memo: ""
        ,memoSub: ""
        ,defaultValue: null // String
        ,uniq: null // String
        ,typeParam: null // String
      },
      fkSearch: {
        kw: ""
        ,cdefs: [
          // { id: 1, name: "t1" } ,{ id: 2, name: "t2" }
        ]
      }
    };

    setInterval(()=>{
        this.refreshDebugBox();
      }, 1000
    );
  }

  refreshDebugBox(){
    $("#debug_box_container").empty().append(
      DebugBox.render(this.state, ()=>{
        __p.render(this.state);
      })
    );
  }

  getTitle(){
    return "カラムの編集";
  }

  updateView(){
    this.refreshDebugBox();
  }

  render(){
    puts("-->> render");
    __g.replaceEl($("#main"), View.render(this.state));
    this.updateView();
  }

  onchange_name(){
    this.state.cdef.name = $("[name=name]").val();
  }

  onchange_pname(){
    this.state.cdef.pname = $("[name=pname]").val();
  }

  onchange_order() {
    const s = $("[name=order]").val();
    this.state.cdef.order = _parseInt(s);
  }

  intOrNull(s){
    if ( /\d+/.test(s) ) {
      return _parseInt(s);
    } else {
      return null;
    }
  }

  onchange_fkTdefId() {
    this.state.cdef.fkTdefId = this.intOrNull(
      $("[name=fk_tdef_id]").val()
    );
  }

  onchange_fkCdefId() {
    this.state.cdef.fkCdefId = this.intOrNull(
      $("[name=fk_cdef_id]").val()
    );
  }

  onchange_memo() {
    this.state.cdef.memo = $("[name=memo]").val();
  }

  onchange_memoSub() {
    this.state.cdef.memoSub = $("[name=memo_sub]").val();
  }

  oninput_fkKeyWord(){
    const kw = $("[name=input_fk_search]").val();
    this.state.fkSearch.kw = kw;

    if (/^\s*$/.test(kw)) {
      return;
    }

      __g.guard();
      __g.api_v2(
        "get", __g.appPath(`/api/rpc`)
        ,{
          action: "fksearch"
          ,kw
        }
        ,(result)=>{
          this.state.fkSearch.cdefs = result.cdefs;
          this.render();
          __g.unguard();
        }
        ,(es)=>{
          __g.printApiErrors(es);
          __g.unguard();
        }
      );
  }

  onchange_pk(ev){
    const checked = MyToggleCheckbox.isChecked(ev);
    this.state.cdef.pk = checked;
    this.render();
  }

  onchange_required(ev){ // TODO rename => not null
    const checked = MyToggleCheckbox.isChecked(ev);
    this.state.cdef.notNull = checked;
    this.render();
  }

  unsignedEnabled(_type){
    if ( _type.match( /(tinyint|smallint|mediumint|int|bigint|double|float|decimal)$/i ) ) {
      return true;
    } else {
      return false;
    }
  }

  change_type(rawType, isCustom){
    const isUnsigned = (
      $("[name=chk_optional_type_unsigned]").prop("checked")
    );

    this.state.cdef.type = rawType;
    this.state.cdef.unsigned = isUnsigned;

    this.render();
  }

  onchange_type() {
    const rawType = $("[name=optional_type]:checked").val();
    if (rawType == null) {
      this.onchange_customType();
    } else {
      this.change_type(rawType, false);
    }
  }

  onclick_typeBtn(type) {
    this.change_type(type, true);
  }

  onchange_customType() {
    const rawType = $("[name=custom_type]").val();
    this.change_type(rawType, true);
  }

  onchange_type_param() {
    this.state.cdef.typeParam = $("[name=type_param]").val();
  }

  onchange_uniq() {
    this.state.cdef.uniq = $("[name=optional_uniq]").val();
  }

  onchange_defaultValue(ev) {
    const val = $(ev.target).val();
    this.state.cdef.defaultValue = (val === "" ? null : val);
  }

  onclick_selectFk(tdefId, cdefId) {
    this.state.cdef.fkTdefId = tdefId;
    this.state.cdef.fkCdefId = cdefId;

    this.render();
  }

  getTdefId() {
    location.href.match( /\/tables\/(\d+)\// );
    return parseInt(RegExp.$1, 10);
  }

  getCdefId() {
    location.href.match( /\/columns\/(\d+)\// );
    return parseInt(RegExp.$1, 10);
  }

  submit(ev) {
    puts("-->> submit", this, ev);

    __g.guard();

    const tdefId = __p.getTdefId();
    const cdefId = __p.getCdefId();

    const cdef = {};
    cdef.id    = cdefId;
    [
      "name", "pname",
      "order",
      "type", "unsigned", "typeParam",
      "notNull", "pk", "uniq", "defaultValue",
      "memo", "memoSub",
      "utime",
      "fkTdefId", "fkCdefId"
    ].forEach(
      key => cdef[key] = this.state.cdef[key]
    );

    const params = {
      cdef
    };

    $("#msg_err").text("");

    const url = __g.appPath(`/api/tables/${tdefId}/columns/${cdefId}`);
    __g.api("patch", url, params, (result)=>{
      puts("ok", result);
      __g.unguard();
      location.href = __g.appPath(`/tables/${tdefId}`);
    }, (errors)=>{
      // puts("ng", errors);
      __g.printApiErrors(errors);
      __g.unguard();
    });
  }

  prepareState(){
    ;
  }

  init(){
    puts("-->> init");

    const tdefId = __p.getTdefId();
    const cdefId = __p.getCdefId();
    const url = __g.appPath(`/api/tables/${tdefId}/columns/${cdefId}`);
    const params = {};

    __g.api("get", url, params, (result)=>{
      Object.assign(__p.state, result);

      this.prepareState();

      this.render(__p.state);

      $("body").on("keydown", (ev)=>{
        const oe = ev.originalEvent;

        if (oe.ctrlKey && oe.key === "Enter") {
          if (window.confirm("保存します")) {
            // 編集中の内容を state に反映させるため
            $(ev.target).blur();

            this.submit();
          }
        }
      });

      __g.unguard();
    }, (errors)=>{
      puts(errors);
      __g.printApiErrors(errors);
      __g.unguard();
    });

  }
};

__g.ready(new Page());
