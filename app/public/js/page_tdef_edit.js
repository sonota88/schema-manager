class View {
  static render(state){
    const table = state.table;

    return TreeBuilder.build(h=> [
      h("h1", {}
      , "テーブルの編集"
      )
    , h("a", { href: `${ __g.appPath('/tables/' + table.id) }` }, "戻る")

    , h("table", { style: { width: "100%" } }
      , h("tr", {}
        , h("th", { style: { "width": "20%" } }, "id")
        , h("td", {}, table.id)
        )
      , h("tr", {}
        , h("th", { style: { "width": "20%" } }, "group id")
        , h("td", {}
          , MySelect.render(__p.formGroups(), {
              onchange: (ev)=>{ __p.onchange_group(ev); }
            , selected: table.groupId
            })
          )
        )
      , h("tr", {}
        , h("th", {}, "name")
        , h("td", {}
          , h("input", { name: "name", value: table.name
              , onchange: ()=>{ __p.onchange_name(); }
              , style: { width: "100%" } 
              }
            )
          )
        )
      , h("tr", {}
        , h("th", {}, "pname")
        , h("td", {}
          , h("input", { name: "pname", value: table.pname || ""
              , onchange: ()=>{ __p.onchange_pname(); }
              , style: { width: "100%" } 
              }
            )
          )
        )
      , h("tr", {}
        , h("th", {}, "memo")
        , h("td", {}
          , h("textarea", {
                name: "memo"
              , onchange: ()=>{ __p.onchange_memo(); }
              , style: { width: "100%", height: "8rem" }
              }
            , table.memo
            )
          )
        )
      , h("tr", {}
        , h("th", {}, "memo sub")
        , h("td", {}
          , h("textarea", {
                name: "memo_sub"
              , onchange: ()=>{ __p.onchange_memoSub(); }
              , style: { width: "100%", height: "8rem" }
              }
            , table.memoSub
            )
          )
        )
      , h("tr", {}
        , h("th", {}, "tags")
        , h("td", {}
          , h("input", {
                name: "tags"
              , value: state.tagNames.join(" ")
              , onchange: ()=>{ __p.onchange_tags(); }
              , style: { width: "100%" }
              }
            )
          )
        )
      )

    , h("button", {
          id: "submit"
        , onclick: (ev)=>{ __p.submit(ev); }
        }
      , "保存"
      )

    , h("hr")
    , DebugBox.render(__p.state)
    ]);
  }
}

class Page {
  constructor(){
    this.state = {
      table: {
        groupId: 1 // selected
      },
      cdefsOld: [],
      tagNames: ["tag1", "tag2"],
      groups: [
        { id: 1, name: "group name" }
      ]
    };
  }

  getTitle(){
    return "テーブルドキュメントの編集";
  }

  render(){
    puts("-->> render");
    $("#main").empty().append(View.render(this.state));
  }

  formGroups() {
    return this.state.groups.map(group => (
      {
        label: group.name, value: group.id
      }
    ));
  }

  onchange_group(ev) {
    this.state.table.groupId = MySelect.getValueAsInt(ev);
  }

  onchange_name(){
    __p.state.table.name = $("[name=name]").val();
    // __p.render();
  }

  onchange_pname(){
    __p.state.table.pname = $("[name=pname]").val();
    // __p.render();
  }

  onchange_memo(){
    __p.state.table.memo = $("[name=memo]").val();
    // __p.render();
  }

  onchange_memoSub(){
    __p.state.table.memoSub = $("[name=memo_sub]").val();
  }

  onchange_tags(){
    const str = $("[name=tags]").val();
    this.state.tagNames = str.split(/ +/);
    // this.render();
  }

  getId(){
    location.href.match( /\/tables\/(\d+)\// );
    return parseInt(RegExp.$1, 10);
  }

  submit(ev){
    puts("-->> submit", this, ev);

    __g.guard();

    const tableId = __p.getId();

    const tdef = {};
    tdef.id      = tableId;
    tdef.groupId = this.state.table.groupId;
    tdef.name    = this.state.table.name;
    tdef.pname   = this.state.table.pname;
    tdef.memo    = this.state.table.memo;
    tdef.memoSub = this.state.table.memoSub;
    tdef.utime   = this.state.table.utime;
    tdef.tags    = this.state.tagNames;

    const params = {
      tdef
    };
    puts(221, params);

    $("#msg_err").text("");

    const url = __g.appPath("/api/tables/" + tableId);
    __g.api("patch", url, params, (result)=>{
      puts("ok", result);
      __g.unguard();
      // TODO utime desc だと便利かも
      location.href = __g.appPath("/tables/" + tableId);
    }, (errors)=>{
      // puts("ng", errors);
      __g.printApiErrors(errors);
      __g.unguard();
    });
  }

  prepareState() {
  }

  init(){
    puts("init", __p.getId());

    const tableId = __p.getId();
    const url = __g.appPath("/api/tables/" + tableId);
    const params = {};

    __g.api("get", url, params, (result)=>{
      puts(197, result.table);
      Object.assign(__p.state, result);

      __p.prepareState();

      __p.render(__p.state);
      __g.unguard();
    }, (errors)=>{
      puts(errors);
      __g.printApiErrors(errors);
      __g.unguard();
    });
  }
};

__g.ready(new Page());
