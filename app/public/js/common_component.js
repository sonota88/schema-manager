class CdefRow {
  static render(cdef, i, opts, onclickDelete, onclickChangeOrder){
    let cdefId;
    if (cdef.id != null) {
      cdefId = cdef.id;
    } else {
      puts("7 to be deprecated");
      cdefId = i + 1;
    }
    const href = __g.appPath(`/tables/${opts.tdefId}/columns/${cdefId}/edit`);

    const isTruthy = (arg)=>{
      if (arg == null
          || arg === false
         ) {
        return false;
      }
      if (/^\s*$/.test(arg.toString())) {
        return false;
      }
      return true;
    };

    const convertPk = (arg)=>{
      if (arg == null || arg === false) {
        return "";
      } else if (/^\s*$/.test(arg)) {
        return `"${arg}"`;
      } else if (arg === true) {
        return "●";
      } else {
        return arg;
      }
    };

    const renderFk = (cdef)=>{
      if (cdef.fkData) {
        let sepWidth;
        if (cdef.fkData.memo == null || cdef.fkData.memo === "") {
          sepWidth = "0";
        } else {
          sepWidth = "0.2rem";
        }

        return TreeBuilder.build(h=>
          h("div", { "class": "memo" }
          , h("span", { style: {
                 display: "inline-block"
                , border: "solid #eee"
                , "border-width": `0 0 ${sepWidth} 0`
                , "margin-bottom": "0.5rem"
                }
              }
            , h("a", { href: __g.appPath(`/tables/${ cdef.fkTdefId }`) }, cdef.fkData.tablePname)
            , ` . ${cdef.fkData.colPname} `
            , h("span", {
                  style: { background: "#eee", padding: "0.1rem" }
                }
              , `(${cdef.fkData.groupName})`
              )
            )
          , h("br")
          , cdef.fkData.memo
          )
        );
      } else {
        return "";
      }
    };

    const renderType = (cdef)=>{
      if (cdef.type == null) {
        return "";
      }

      if (cdef.unsigned) {
        return TreeBuilder.build(h=> [
          cdef.type
        , " "
        , h("span", { style: {
               color: "#370"
             , background: "#e8f8e0"
             , padding: "0.05rem 0.3rem"
             , "border-radius": "0.2rem"
             , display: "inline-block"
             , "font-size": "80%"
             }
           }
          , "unsigned"
          )
        ]);
      } else {
        return cdef.type;
      }
    };

    return TreeBuilder.build(h=>
      h("tr", {}
      , h("th", {}, cdefId
        // , h("br")
        , " "
        , opts.editLink
          ? h("a", { href: href
              , style: { "font-size": "80%" }
              }, "編集"
            )
          : null
        )
      , h("td", { style: {
              opacity: "0.5"
            , "font-size": "80%"
            }
          }
        , (i + 1)
        , ":"
        , cdef.order
        )
      , h("td", {}, cdef.name)
      , h("td", {}, cdef.pname)
      // , h("td", {}, (isTruthy(cdef.pk) ? "●" : ""))
      , h("td", { title: "pk" }, convertPk(cdef.pk))
      , h("td", { title: "not null" }, cdef.notNull ? "●" : "")
      , h("td", {}, cdef.uniq)
      , h("td", {}, renderType(cdef))
      , h("td", { title: "type param" }, cdef.typeParam)
      , h("td", { title: "default value" }, cdef.defaultValue)
      , h("td", { "class": "memo_cell" }, __g.renderMemo(cdef.memo))
      , h("td", { "class": "my_memo_cell" }, __g.renderMemo(cdef.memoSub))
      , h("td", { title: "fk", "class": "memo_cell" }, renderFk(cdef))
      , h("td", {}
        , h("button", {
              onclick: ()=>{ onclickDelete(cdefId); }
            }, "削除"
          )
        , h("button", {
              onclick: ()=>{ onclickChangeOrder(cdefId); }
            }, "↑↓"
          )
        )
      )
    );
  }
}

class CdefsTable {
  static render(cdefs, opts, onclickDelete, onclickChangeOrder){
    opts = opts || {
      //   editLink: true
      // , tdefId: 123
    }
    return TreeBuilder.build(h=>
      h("table", { "class": "hover_hl cdef_table" }
      , h("tr", {}
        , h("th", {
              style: { width: "4rem" }
            }, "#"
          )
        , h("th", {}, "order")
        , h("th", {
              style: { "min-width": "6rem" }
            }
          , "name"
          )
        , h("th", {}, "pname")
        , h("th", {}, "pk")
        , h("th", { title: "not null" }, "NN")
        , h("th", { title: "unique" }, "UNQ")
        , h("th", {}, "type")
        , h("th", {}, "type param")
        , h("th", {}, "default")
        , h("th", {}, "memo")
        , h("th", {}, "my memo")
        , h("th", {}, "fk")
        , h("th", {}, "")
        )
      , cdefs
        .map((cdef)=>{
          if (cdef == null) {
            puts("83 to_be_deprecated");
            return { order: 0 };
          } else {
            return cdef;
          }
        })
        .sort((a, b)=>{
          // asc
          return a.order - b.order;
        })
        .map((cdef, i) => {
          return CdefRow.render(cdef, i, opts, onclickDelete, onclickChangeOrder);
        })
      )
    );
  }
}

class DebugBox {
  static render(data, renderFn){
    function getClass(flag){
      if (data.foldDebugBox == null) {
        // 初期状態
        return "debug_box_fold";
      } else if (data.foldDebugBox) {
        return "debug_box_fold";
      } else {
        return"debug_box_expand";
      }
    }

    function toggleFold(){
      if (data.foldDebugBox == null) {
        data.foldDebugBox = false;
      } else {
        data.foldDebugBox = !data.foldDebugBox;
      }
      if (renderFn) { renderFn(); }
    }

    return TreeBuilder.build(h=>
      h("div", {
          "class": "box"
        , id: "debug_box_fold_area"
        }
      , h("button", {
            onclick: toggleFold
          }
        , "fold=" + data.foldDebugBox
        )

      , h("pre"
        , { "class": "debug_box " + getClass() }
        , "// state (for debug)\n\n"
        , tojson_pretty(data)
        )
      )
    );
  }
}

class MyCheckbox {
  static render(text, checked, customAttrs){
    const attrs = Object.assign({
      type: "checkbox"
    }, customAttrs);
    const classList = ["label_hl"];

    if (checked) {
      attrs.checked = "checked";
      classList.push("label_selected");
    }

    return TreeBuilder.build(h=>
      h("span", { "class": "mycheckboxes_container" }
      , h("label", { "class": classList.join(" ") }
        , h("input", attrs)
        , text
        )
      )
    );
  }
}

class MyCheckboxes {
  static getValues(ev){
    const $tgt = $(ev.target);
    const $cont = $tgt.closest(".mycheckboxes_container");

    return Array.from($cont.find("input:checked"))
      .map(input => input.value);
  }
}

// --------------------------------

class MyRadio {
  static render(name, item, selectedVal){
    const selected = (item.value === selectedVal);

    const labelClasses = ["container_label"];
    if (selected) { labelClasses.push("container_label_selected"); }

    const attrs = {
      type: "radio",
      name: name,
      value: item.value
    };
    if (selected) {
      attrs.checked = "checked";
    }

    return TreeBuilder.build(h =>
      h("label", { "class": labelClasses.join(" ") }
      , h("input", attrs)
      , item.text
      )
    );
  }
}

/*
  opts:
    selected: single value
    onchange: (ev) => { ... }
*/
class MyRadioGroup {
  static render(name, items, opts){
    return TreeBuilder.build(h =>
      h("span", {
          "class": "myradiogroup_container"
        , onchange: opts.onchange
        }
      , items.map(item =>
          MyRadio.render(name, item, opts.selected)
        )
      )
    );
  }

  static getValue(ev){
    const $tgt = $(ev.target);
    const $cont = $tgt.closest(".myradiogroup_container");

    return $cont.find("input:checked").val();
  }

  static getValueAsInt(ev){
    const val = MyRadioGroup.getValue(ev);
    return parseInt(val, 10);
  }
}

// --------------------------------

class MySelect {
  static render(items, opts){
    const attrs = {};
    if ("name" in opts) {
      attrs.name = opts.name;
    }
    if ("onchange" in opts) {
      attrs.onchange = opts.onchange;
    }

    return TreeBuilder.build(h =>
      h("select", attrs
      , items.map(item => {
            const optAttrs = { value: item.value };
            if (item.value === opts.selected) {
              optAttrs.selected = "selected";
            }
            return h("option", optAttrs, item.label);
          }
        )
      )
    );
  }

  static getValue(ev) {
    return ev.target.value;
  }

  static getValueAsInt(ev) {
    const val = MySelect.getValue(ev);
    return parseInt(val, 10);
  }
}

// --------------------------------

class MyToggleCheckbox {
  static render(checked, text, onchange){
    const labelClasses = ["container_label"];
    if (checked) { labelClasses.push("container_label_selected"); }

    const attrs = {
      type: "checkbox",
      onchange
    };
    if (checked) { attrs.checked = "checked"; }

    return TreeBuilder.build(h =>
      h("label", { "class": labelClasses.join(" ") }
      , h("input", attrs)
      , text
      )
    );
  }

  static isChecked(ev){
    return $(ev.target).prop("checked");
  }
}
