function puts(... args){
  console.log.apply(console, args);
}

const tojson = (x)=> JSON.stringify(x);
const tojson_pretty = (x)=> JSON.stringify(x, null, "  ");
const _parseInt = (x)=> parseInt(x, 10);

const __g = {
  CONTEXT_PATH: "table_manager"

  ,api: function(method, path, data, fnOk, fnNg){
    var _data = {
      _method: method.toUpperCase()
      ,_params: JSON.stringify(data)
    };
    $.post(path, _data, (data)=>{
      if(data.errors.length > 0){
        fnNg(data.errors);
        return;
      }
      fnOk(data.result);
    });
  },

  api_v2: (method, path, data, fnOk, fnNg)=>{
    const req = new Request(path);

    const fd = new FormData();
    fd.append("_method", method.toUpperCase());
    fd.append("_params", JSON.stringify(data));

    fetch(req, {
      method: 'POST',
      body: fd,
      credentials: 'include', // cookie をリクエストに含める
    }).then((res)=>{
      if (res.ok) {
        puts("res.ok == true", res);
      } else {
        puts("res.ok != true", res);
      }
      return res.json();
    }).then((resData)=>{
      if (resData.errors.length > 0) {
        fnNg(resData.errors);
        return;
      }
      fnOk(resData.result);
    }).catch((err)=>{
      puts(err);
    });
  },

  guard: ()=>{
    $("#guard_layer").show();
  },

  unguard: ()=>{
    setTimeout(()=>{
      $("#guard_layer").fadeOut(100);
    }, 100);
  },

  printApiErrors: (es)=>{
    es.forEach((e, i)=>{
      puts(`-------- error ${i} --------`);
      puts(e.trace.split("\n").reverse().join("\n"));
      puts(e.msg);
    });
  },

  setTitle: (title)=>{
    document.title = title + " | table def manager";
  },

  ready: (page)=>{
    window.__p = page;
    document.addEventListener("DOMContentLoaded", ()=>{
      page.init();
      __g.setTitle(page.getTitle());
    });
  }

  ,printApiErrors: (es)=>{
    const lines = [];
    es.forEach((e, i)=>{
      lines.push(`-------- error ${i} --------`);
      e.trace.split("\n").reverse()
        .filter(line => {
          return (! /\/vendor\/bundle\/ruby\//.test(line))
          && (! /\/\.rbenv\/versions\//.test(line))
        })
        .forEach(line => lines.push("    " + line));
      lines.push(e.msg);
    });
    const msg = lines.map(line => line + "\n").join("");

    puts(msg);

    const $el = $("#err_msg");
    if ($el.length > 0) {
      $el.text(msg);
      $el.show();
    }
  },

  getUrlTail: ()=>{
    const url = new URL(location.href);
    return url.pathname;
  },

  getApiUrl: ()=>{
    return "/api" + __g.getUrlTail();
  },

  appPath: (rpath)=>{
    return rpath;
  },

  // view helper
  vh_renderPagination: (searchCond)=>{
    let hs = [];
    hs.push( '<td><a href="./">first</a></td>' );
    if(searchCond.pagePrev){
      hs.push( '<td><a href="./?p=' + searchCond.pagePrev + '">prev</a></td>' );
    }else{
      hs.push( '<td>-</td>' );
    }
    hs.push( '<td>' + searchCond.page + '</td>' );
    if(searchCond.pageNext){
      hs.push( '<td><a href="./?p=' + searchCond.pageNext + '">next</a></td>' );
    }else{
      hs.push( '<td>-</td>' );
    }
    return '<table><tr>' + hs.join("") + '</tr></table>';
  }

  ,replaceEl: (parentSel, child)=>{
    const $parent = $(parentSel);
    // puts(157, $parent, $parent.length, typeof $parent);
    if ( $parent.length === 0 ) {
      throw "invalid parent";
    }
    $parent.empty().append(child);
  },

  renderMemo: (memo)=>{
    const _memo = memo || "";
    const _build = TreeBuilder.build;
    let buf = "";
    let pos = 0;
    const els = [];
    while(pos < _memo.length){
      const tail = _memo.substring(pos);
      if ( tail.match(/^\n/) ) {
        els.push(buf);
        els.push(_build(h => h("br")));
        buf = "";
        pos += 1;
      } else if ( tail.match(/^(https?:\/\/(.+))/) ) {
        const url = RegExp.$1;
        els.push(buf);
        els.push(_build(h =>
          h("a", { href: url }, url)
        ));
        buf = "";
        pos += url.length;
      } else {
        buf += tail.charAt(0);
        pos += 1;
      }
    }
    if (buf.length > 0) {
      els.push(buf);
    }
    // puts(171, tojson(_memo), els);
    
    return TreeBuilder.build(h =>
      h("div", { "class": "memo" }, els)
    );
  }

  ,isBlank(arg){
    if (arg == null) {
      return true;
    }
    if (/^ *$/.test(arg)) {
      return true;
    }
  },

  _debounceMap: {}, // fn => timer
  debounce(fn, msec){
    if(__g._debounceMap[fn] != null){
      clearTimeout(__g._debounceMap[fn]);
    }

    __g._debounceMap[fn] = setTimeout(
      ()=>{
        fn();
        __g._debounceMap[fn] = null;
      },
      msec
    );
  },

  // input[type=text] を対象に使う
  selectInputText(ev){
    ev.target.select();
  },

  dumpState() {
    console.log(
      JSON.stringify(__p.state, null, "    ")
    );
  }
};
