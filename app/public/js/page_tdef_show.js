const h = TreeBuilder.h;

const COLUMN_JSON_TEMPLATE = `
  {
    "order": 10000,
    "id": null,
    "name": "c1",
    "pname": "c1",
    "memo": "",
    "memoSub": "",
    "notNull": null,
    "pk": null,
    "type": "text",
    "unsigned": null,
    "ctime": "2021-01-01_12:00:00",
    "utime": "2021-01-01_12:00:00",
    "typeParam": null,
    "uniq": null,
    "default_value": null,
    "fkTdefId": null,
    "fkCdefId": null
  }
`;

class TableTagLink {
  static render(name){
    return (
      h("span", {}
      , h("a", {
          "class": "tag_link"
        , href: __g.appPath(`/tables/?t=${name}`)
        }, name)
      )
    );
  }
}

class Erd {
  static render(state) {
    const tdef = state.table;

    let imgPath = `/img/erd/tables/${tdef.id}?numHops=${state.erd.numHops}`;
    imgPath += `&keyOnly=` + (state.erd.keyOnly ? "1" : "0");

    return (
      h("div", {}

      , "hide" // TODO
      , " / "
      , "dir" // TODO
      , " / "
      , "cluster" // TODO 他のクラスタを表示しない or する
      , " / "
      , MyToggleCheckbox.render(
          state.erd.keyOnly
        , "key only"
        , (ev)=>{ __p.onchange_erd_keyOnly(ev); }
        )

      , " / hops: "
      , MyRadioGroup.render(
          "erd_num_hops"
        , [
            { text: "1", value: 1 },
            { text: "2", value: 2 },
          ]
        , { onchange: (ev)=>{ __p.onchange_erd_numHops(ev); }
          , selected: state.erd.numHops
          }
        )

      , h("br")
      , h("img", {
          src: __g.appPath(imgPath)
        , style: { border: "solid 1px #aaa" }
        })
      )
    );
  }
}

class UsingTables {
  static render(state) {
    const ts = state.usingTdefs;

    return (
      h("div", {}
        , ts.map(t =>
          h("p", {}
          , "- "
          , h("a", { href: __g.appPath(`/tables/${t.id}`) }, t.pname)
          )
        )
      )
    );
  }
}

class ColDiff {
  static render(colDiff) {
    colDiff.cdef.order = colDiff.order; // TODO

    return (
      h("div", { style: { border: "solid 0.2rem #484" } }
      , h("h2", {}, `${colDiff.order}: ${colDiff.cdef.pname} / ${colDiff.cdef.name}`)
      , h("pre", {
            style: {
              background: "#f4f4f0"
            , padding: "0.5rem"
            , fontSize: "90%"
            }
          }, __p.colorizeDiff(colDiff.diff)
        )
      , h("textarea"
        , {
            style: { width: "50%", height: "10rem" },
            readonly: "readonly"
          }
        , JSON.stringify(colDiff.cdef, null, "  ")
        )
      , h("br")
      , h("button", {
            onclick: ()=>{ __p.onclick_addColumnUsingJson_v2(colDiff.cdef) }
          }
        , "add"
        )
      )
    );
  }
}

class View {
  static deleteCdef(tdefId, cdefId){
    if (!confirm("削除しますか？")) {
      return;
    }

    __g.guard();

    __g.api(
      "delete", __g.appPath(`/api/tables/${tdefId}/columns/${cdefId}`),
      {},
      (result)=>{
        location.reload();
      }, (errors)=>{
        puts(errors);
        __g.printApiErrors(errors);
        __g.unguard();
      }
    );
  }

  static changeOrder(tdefId, cdefId){
    const newOrderStr = prompt("Input new order", "");
    if (newOrderStr === "") {
      return;
    }
    const newOrder = _parseInt(newOrderStr);
    // TODO should not be NaN

    __g.guard();

    __g.api(
      "get", __g.appPath(`/api/rpc`),
      {
        action: "patch_cdef",
        tdefId,
        cdefId,
        cdef: {
          order: newOrder
        }
      },
      (result)=>{
        location.reload();
      }, (errors)=>{
        puts(errors);
        __g.printApiErrors(errors);
        __g.unguard();
      }
    );
  }

  static render(state){
    const tdef = state.table;
    const cdefOpts_v1 = { // to_be_deprecated
      editLink: false
    , tdefId: tdef.id
    }
    const cdefOpts = {
      editLink: true
    , tdefId: tdef.id
    }

    return ([
      h("h1", {}
      , `テーブル定義詳細 (${tdef.id}) ${tdef.pname} / ${tdef.name}`
      )

    /*
    , h("h2", {}, "ER図")
    , Erd.render(state)
    , h("br")
    */

    , h("a", { href: __g.appPath('/tables/') }, "戻る")
    , " - "
    , h("a", { href: __g.appPath('/tables/' + tdef.id + '/edit') }, "編集")

    , h("table", {}
      , h("tr", {}
        , h("th", {}, "group")
        , h("td", {}
          , h("a", { href: __g.appPath(`/tables/?g=${tdef.groupId}`) }
            , `(${tdef.groupId}) ${__p.getGroupName(tdef.groupId)}`
            )
          )
        )
      , h("tr", {}
        , h("th", {}, "ctime / utime")
        , h("td", {}, `${tdef.ctime} / ${tdef.utime}`)
        )
      , h("tr", {}
        , h("th", {}, "memo")
        , h("td", {}, __g.renderMemo(tdef.memo))
        )
      , h("tr", {}
        , h("th", {}, "memo sub")
        , h("td", {}, __g.renderMemo(tdef.memoSub))
        )
      , h("tr", {}
        , h("th", {}, "tag")
        , h("td", {}
          , state.tagNames.map(tagName => TableTagLink.render(tagName))
          )
        )
      )

    , h("h2", {}, "カラム")
    , CdefsTable.render(state.cdefs, cdefOpts
      , (cdefId)=>{ View.deleteCdef(tdef.id, cdefId); }
      , (cdefId)=>{ View.changeOrder(tdef.id, cdefId); }
      )
    , h("button", {
          onclick: ()=>{ __p.onclick_addColumn(); }
        }, "カラム追加"
      )

    , h("h2", {}, "利用しているテーブル")
    , "以下のテーブルから参照されています:"
    , UsingTables.render(state)

    , h("h2", {}, "DDL")
    , "※ DDL生成機能は不完全です"
    , h("br")
    , h("textarea", {
          style: { width: "50%", height: "6rem" }
        }
      , __p.generateDdl()
      )

    , h("h2", {}, "雑")

    , "カラム 縦"
    , h("br")
    , h("textarea", {}
      , __p.sortCdefs(state.cdefs.filter(cdef => cdef != null))
          .map(cdef => cdef.pname + "\n")
          .join("")
      )

    , h("hr")
    , "カラム TSV"
    , h("br")
    , h("textarea", { style: { width: "80%" } }
      , __p.sortCdefs(state.cdefs.filter(cdef => cdef != null))
          .map(cdef => cdef.pname)
          .join("\t")
      )

    , h("hr")
    , "カラム CSV"
    , h("br")
    , h("textarea", { style: { width: "80%" } }
      , __p.makeCsv(state.cdefs)
      )

    , h("hr")
    , "UPDATE文"
    , h("br")
    , h("textarea", {
          // style: { width: "80%" , height: "80%" }
        }
      , __p.generateUpdateStmt()
      )

    , h("hr")
    , "カラム追加"
    , h("br")
    , h("textarea", {
          "class": "add_column_json"
        , style: { width: "80%", height: "8rem" }
        }
      , COLUMN_JSON_TEMPLATE
      )
    , h("br")
    , h("button", {
        onclick: ()=>{ __p.onclick_addColumnUsingJson(); }
      }
    , "追加")

    , h("hr")
    , "diff"
    , h("br")
    , h("textarea", {
          "class": "tdef_compare_other"
        , onchange: ()=>{ __p.onchange_tdefOther(); }
        }
      , state.tdefOther
      )
    , h("br")
    , h("button", { onclick: ()=>{ __p.onchange_tdefOther(); } }, "compare")
    , h("div", { "class": "tdef_compare_temp", style: { display: "Xnone" } })

    , h("textarea", {
          style: { width: "80%", height: "4rem" }
        }
      , JSON.stringify(state.tdefCompareInput, null, "  ")
      )
    , h("pre", {
          style: {
            background: "#f4f4f0"
          , padding: "0.5rem"
          , fontSize: "90%"
          }
        }
      , __p.colorizeDiff(state.tdefCompareResult)
      )
    , h("div", {}
      , state.tdefCompareDiffCols.map(ColDiff.render)
      )

    , h("hr")
    // , DebugBox.render(state)
    , h("div", { id: "debug_box_container" })
    ]);
  }
}

// test data
/*

<tr>
  <td>c1</td>
  <td>カラム名</td>
  <td>○</td> <!-- pk -->
  <td></td> <!-- fk -->
  <td></td> <!-- uk -->
  <td>○</td> <!-- ai -->
  <td>○</td> <!-- not null -->
  <td>INT</td> <!-- type -->
  <td>11</td> <!-- length -->
  <td>-1</td> <!-- default -->
  <td>UNSIGNED</td> <!-- attributes -->
  <td><p>comment</p><p>comment</p></td> <!-- comment -->
  <td>comment2</td> <!-- comment2 -->
</tr>
<tr>
  <td>c2</td>
  <td><p>a</p><p>b</p></td>
  <td>false</td>
</tr>

*/

class TdefCompare {
  static innerText(el) {
    let s = "";

    el.childNodes.forEach(cn => {
      if (cn.nodeType === 3) {
        // TEXT_NODE
        s += cn.textContent;
      } else if (cn.nodeType === 1) {
        // ELEMENT_NODE
        s += this.innerText(cn);
        if (cn.tagName === "P") {
          s += "\n";
        }
      } else {
        s += $(cn).text();
      }
    });

    return s;
  }

  static preproc(input) {
    // Do some preprocessing here
    // const names = ["pname", "type", "not_null"];
    const names = ["pname", "name", "pk", "fk", "uk", "ai", "not_null", "type", "type_param", "default_value", "attr", "comment", "note"];

    const $el = $(".tdef_compare_temp");
    $el.empty().append('<table>' + input + '</table>'); // set as HTML

    // puts(182, $el.find("tr").get());
    const cols = [];
    $el.find("tr").get().forEach(tr =>{
      const obj = {};
      $(tr).find("td").get().map((td, i) =>{
        const k = names[i];
        obj[k] = this.innerText(td).trim();
      });

      obj["memo"] = obj["memo"] || "";
      obj["memo_sub"] = "";

      cols.push(obj);
    });
    // puts(388, cols);

    return { cdefs: cols };
  }
}

// TODO 共通の場所に移動すると良さそう
class Cdef {
  constructor(data) {
    this.data = data;
  }

  getPname() { return this.data.pname; }
  isPk() {
    return JSON.parse(this.data.json).pk;
  }
  isRequired() {
    return JSON.parse(this.data.json).notNull;
  }
}

class Page {
  constructor(){
    this.state = {
      table: { pname: "..." },
      erd: {
        keyOnly: true, // キーカラムだけを表示
        numHops: 1
      },
      usingTables: [
        { id: 1, pname: "t1" },
        { id: 2, pname: "t2" },
      ],
      tdefOther: "...",
      tdefCompareResult: "...",
      tdefCompareDiffCols: [], // [ { cdef: { ... }, order: 10, diff: "..." } ]
      tdefCompareInput: "...",
    };
  }

  getTitle() {
    return `テーブル定義の詳細: ${this.state.table.pname}`;
  }

  refreshDebugBox(){
    $("#debug_box_container").empty().append(
      DebugBox.render(this.state, ()=>{
        __p.render();
      })
    );
  }

  updateView(){
    this.refreshDebugBox();
  }

  render(){
    __g.replaceEl("#main", View.render(this.state));
    __g.setTitle(__p.getTitle());
    this.updateView();
  }

  getId(){
    location.href.match( /\/tables\/(\d+)$/ );
    return parseInt(RegExp.$1, 10);
  }

  getTdefOtherKey() {
    return `tdef_other_${this.state.table.id}`;
  }

  prepareState(){
    // fetch fk
    this.state.cdefs
      .filter(cdef => cdef.fkTdefId && cdef.fkCdefId)
      .forEach(cdef => {
        __g.api(
          "get", __g.appPath(`/api/tables/${cdef.fkTdefId}/columns/${cdef.fkCdefId}`),
          { withTdef: true },
          (result)=>{
            const fkCdef = result.cdef;
            const fkTdef = result.tdef;
            cdef.fkData = {
              tablePname: fkTdef.pname
              ,colPname: fkCdef.pname
              ,groupName: this.getGroupName(fkTdef.groupId)
              ,memo: fkCdef.memo
            };
            __p.render(__p.state);
          }, (errors)=>{
            __g.printApiErrors(errors);
          });
      });

    if (this.getTdefOtherKey() in sessionStorage) {
      this.state.tdefOther = sessionStorage.getItem(this.getTdefOtherKey());
    }
  }


  init(){
    puts("init", __p.getId());

    __g.api("get", __g.getApiUrl(), {}, (result)=>{
      puts(20, result);

      for (let k of Object.keys(result)) {
        __p.state[k] = result[k];
      }

      this.prepareState();

      __p.render(__p.state);
      __g.unguard();
    }, (errors)=>{
      puts(errors);
      __g.printApiErrors(errors);
      __g.unguard();
    });
  }

  // handlers

  onclick_addColumn(){
    __g.guard();

    const tdefId = __p.getId();

    __g.api(
      "post", __g.appPath(`/api/tables/${tdefId}/columns/`),
      {},
      result => {
        location.reload();
      },
      errors => {
        puts(errors);
        __g.printApiErrors(errors);
        __g.unguard();
      }
    );
  }

  onchange_tdefOther(ev) {
    this.state.tdefOther = $(".tdef_compare_other").val();
    sessionStorage.setItem(this.getTdefOtherKey(), this.state.tdefOther);

    __g.api(
      "get", __g.appPath(`/api/rpc`),
      {
        action: "diff_tdef",
        tdef: this.state.table,
        cdefs: this.state.cdefs,
        tdefOther: TdefCompare.preproc(this.state.tdefOther),
      },
      (result)=>{
        // puts(264, result.diff);
        this.state.tdefCompareResult = result.diff;
        this.state.tdefCompareDiffCols = result.diffCols;
        this.state.tdefCompareInput = result.input;
        this.render();
      },
      (errors)=>{
        __g.printApiErrors(errors);
      }
    );
  }

  onchange_erd_keyOnly(ev) {
    const val = MyToggleCheckbox.isChecked(ev);
    this.state.erd.keyOnly = val;
    __p.render(__p.state);
  }

  onchange_erd_numHops(ev) {
    const val = MyRadioGroup.getValueAsInt(ev);
    this.state.erd.numHops = val;
    __p.render(__p.state);
  }

  onclick_addColumnUsingJson() {
    const json = $(".add_column_json")[0].value;
    puts("484 TODO", json);
    const _cdef = JSON.parse(json);

    puts("-->> addColumnUsingJson");
    this.addColumn(_cdef);
  }

  onclick_addColumnUsingJson_v2(cdef) {
    this.addColumn(cdef);
  }

  // methods

  addColumn(_cdef) {
    __g.guard();

    const tdefId = this.getId();

    const cdef = {};
    cdef.id = null;
    [
      "name", "pname",
      "order",
      "type", "unsigned", "typeParam",
      "notNull", "pk", "uniq", "defaultValue",
      "memo", "memoSub",
      "fkTdefId", "fkCdefId"
    ].forEach(
      key => cdef[key] = _cdef[key]
    );

    const params = {
      cdef
    };

    $("#msg_err").text("");

    __g.api(
      "post", __g.appPath(`/api/tables/${tdefId}/columns/`),
      params,
      result => {
        location.reload();
      },
      errors => {
        puts(errors);
        __g.printApiErrors(errors);
        __g.unguard();
      }
    );
  }

  getGroupName(groupId) {
    const group = this.state.groups.find(g => g.id === groupId);
    if (group) {
      return group.name;
    } else {
      return "?";
    }
  }

  sortCdefs(cdefs) {
    return cdefs.sort((a, b) => a.order - b.order);
  }

  makeCsv(cdefs) {
    const head =
      this.sortCdefs(
        cdefs.filter(cdef => cdef != null)
      )
        .map(cdef => `"${cdef.pname}"`)
        .join(",")
    ;
    const vals = []
    cdefs.forEach((cdef, i)=> {
      if (cdef.type === "datetime" || cdef.type === "timestamp") {
        vals.push(`"2021-01-02 12:00:00"`);
      } else if (cdef.type === "date") {
        vals.push(`"2021-01-02"`);
      } else if (cdef.type === "varchar") {
        vals.push(`"s${i + 1}"`);
      } else {
        vals.push(`"${i + 1}"`);
      }
    });

    return [
      head,
      vals.join(",")
    ]
      .map(line => line + "\n")
      .join("")
    ;
  }

  generateDdl(){
    return this.state.ddl;
  }

  generateUpdateStmt() {
    const table = this.state.table.pname;
    const cdefs = this.sortCdefs(this.state.cdefs);

    const cond = 
      cdefs
        .map(cdefRaw => {
          const cdef = new Cdef(cdefRaw);
          let cond = "";
          if (! cdef.isPk()) {
            cond += ` -- `;
          }
          cond += `and ${cdef.getPname()} =  -- `;
          if (cdef.isPk()) {
            cond += ` pk`;
          }
          if (cdef.isRequired()) {
            cond += ` not_null`;
          }
          return cond;
        })
        .join("\n  ");

    const set =
      cdefs
        .map(cdefRaw => {
          const cdef = new Cdef(cdefRaw);
          let line = `${cdef.getPname()} =  --`;
          if (cdef.isRequired()) {
            line += ` not_null`;
          }
          return line;
        })
        .join("\n  ,");

    const cols =
      cdefs
        .map(raw => {
          const cdef = new Cdef(raw);
          return cdef.getPname();
        })
        .join(", ");

    return `
select ${cols}
from ${table}
where 1 == 1
  ${cond}
;

update ${table}
set
  ${set}
where 1 == 1
  ${cond}
;
    `.trim() + "\n";
  }

  colorizeDiff(src) {
    return src
      .split("\n")
      .map(line => line + "\n")
      .map(line => {
        if (line.startsWith("-")) {
          return h("span", { style: { color: "#d00" } }, line);
        } else if (line.startsWith("+")) {
          return h("span", { style: { color: "#280" } }, line);
        } else {
          return line;
        }
      });
  }
}

__g.ready(new Page());
