const h = TreeBuilder.h;

class GroupRow {
  static render(group){
    return (
      h("tr", {}
      , h("td", {}
        , group.id
        )
      , h("td", {}
        , h("a", { href: __g.appPath(`/tdefs/?g=${group.id}`) }
          , group.name
          )
        )
      , h("td", {}
        , group.memo
        )
      )
    );
  }
}

class View {
  static render(state){
    return (
      h("div", {}

, h("table", { "class": "hover_hl" }
  , h("tr", {}
    , h("th", {}, "id")
    , h("th", {}, "name")
    , h("th", {}, "memo")
    )
  , state.groups.map(GroupRow.render)
  )

      , h("div", { id: "debug_box_outer" })
      )
    );
  }
}

class Page {
  constructor(){
    this.state = {};
  }

  getTitle(){
    return "グループの一覧";
  }

  render(){
    __g.replaceEl("#main", View.render(this.state));
    __g.replaceEl("#debug_box_outer", DebugBox.render(this.state));
  }

  prepareState(){
    ;
  }

  init(){
    const params = {};

    __g.api("get", __g.getApiUrl(), params, (result)=>{
      puts(result);
      this.state = result;

      this.prepareState();

      this.render();
      __g.unguard();
    }, (errors)=>{
      __g.unguard();
      __g.printApiErrors(errors);
    });
  }

}

__g.ready(new Page());
