class View {
  static render(state){
    return TreeBuilder.build(h =>
      h("div", {}
      , h("hr")

      , h("button", {
            onclick: ()=>{ __p.onclick_updateTdbData(); }
          }
        , "update tdb data"
        )

      , h("hr")

      , h("button", {}, "OK")
      , h("input", { type: "checkbox" })
      , h("a", { href: "#" }, "link " + state.val)
      , h("button", {
            onclick: ()=>{ __p.onclick_reloadLibs(); }
          }
        , "reload libs"
        )
      )
    );
  }
}

class Page {
  constructor(){
    this.state = {};
  }

  getTitle(){
    return "sinatra-skelton";
  }

  onclick_reloadLibs(){
    __g.api_v2("get", "/api/reload_libs", {}, (result)=>{
      __g.unguard();
    }, (errors)=>{
      __g.unguard();
      __g.printApiErrors(errors);
      alert("Check console.");
    });
  }

  init(){
    puts("init");
    // __g.api_v2("get", "/api/sample", { a: 123, b: { c: 456 } }, (result)=>{
    //   __g.unguard();
    //   puts(result);
    //   Object.assign(this.state, result);

    //   this.render();

    // }, (errors)=>{
    //   __g.unguard();
    //   __g.printApiErrors(errors);
    //   alert("Check console.");
    // });

    this.render();

    __g.unguard();
  }

  render(){
    $("#tree_builder_container")
      .empty()
      .append(View.render({ val: 234 }));
  }

  onclick_updateTdbData(){
    __g.api_v2(
      "get", __g.appPath(`/api/rpc`)
      ,{
        action: "update_tdb_data"
      }
      ,(result)=>{
        __g.unguard();
      }
      ,(es)=>{
        __g.printApiErrors(es);
        __g.unguard();
      }
    );
  }
}

__g.ready(new Page());
