require "webrick"
require "pp"
require "json"

require_relative "lib"

config = JSON.parse(File.read("config.json"))
APP_DIR = File.expand_path(config["app_dir"])
DATA_DIR = File.expand_path(config["data_dir"])
WEBLIB_DIR = File.expand_path(File.join(APP_DIR, "../weblib"))

server = WEBrick::HTTPServer.new({
  # DocumentRoot: './',
  BindAddress:  "127.0.0.1",
  Port:         9004
})

server.mount("/", MyServlet)

trap("INT") { server.shutdown }

server.start
